//
//  PipotronGeneratorTests.swift
//
//
//  Created by Joël Brogniart on 28/03/2024.
//

import Testing
@testable import StringGenerator

@Suite("Pipotron generator tests")
struct PipotronGeneratorTests {
    let generator = PipotronGenerator()
    let expectedGenerator = String.Generator.pipotron
    
    @Test("Generator name is correct")
    func generatorNameIsCorrect() {
        #expect(generator.name == expectedGenerator)
    }
    
    @Test("Generator generate should not return empty string")
    func generatorGenerateShouldNotReturnEmptyString() {
        #expect(generator.generate().isEmpty == false)
    }
}
