//
//  LoremipsumGeneratorTests.swift
//  StringGenerator
//
//  Created by Joël Brogniart on 05/11/2024.
//

import Testing
@testable import StringGenerator

@Suite("Loremipsum generator tests")
struct LoremipsumGeneratorTests {
    let generator = LoremipsumGenerator()
    let expectedGenerator = String.Generator.loremipsum
    
    @Test("Generator name is correct")
    func generatorNameIsCorrect() {
        #expect(generator.name == expectedGenerator)
    }
    
    @Test("Generator generate should not return empty string")
    func generatorGenerateShouldNotReturnEmptyString() {
        #expect(generator.generate().isEmpty == false)
    }
}
