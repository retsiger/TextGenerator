//
//  ValeriGeneratorTests.swift
//  StringGenerator
//
//  Created by Joël Brogniart on 26/11/2024.
//

import Testing
@testable import StringGenerator

@Suite("Valeri generator tests")
struct ValeriGeneratorTests {
    let generator = ValeriGenerator()
    let expectedGenerator = String.Generator.valeri
    
    @Test("Generator name is correct")
    func generatorNameIsCorrect() {
        #expect(generator.name == expectedGenerator)
    }
    
    @Test("Generator generate should not return empty string")
    func generatorGenerateShouldNotReturnEmptyString() async throws {
        #expect(generator.generate().isEmpty == false)
    }

    @Test("Conjuguer être doit donner les bonnes formes")
    func conjuguerEtreDoitDonnerLesBonnesFormes() async throws {
        let verbe = "être"
        let genre = "H"
        let expectedForms = [
            ["étais", "étais", "était", "étions", "étiez", "étaient"],
            ["suis", "es", "est", "sommes", "êtes", "sont"],
            ["serai", "seras", "sera", "serons", "serez", "seront"],
            ["été", "étant"]
        ]
        for temps in 1...3 {
            for personne in 1...6 {
                let forme = generator.conjuguer(verbe, temps: temps, pers: [personne], questionInv: false, genreSujet: genre)
                #expect(forme == expectedForms[temps - 1][personne - 1])
            }
        }
        /*
         // Je dois fouiller dans le code plus profondément pour comprendre comment marchent
         // les participes.
         let temps = 4
         for personne in 1...2 {
         let forme = generator.conjuguer(verbe, temps: temps, pers: [personne], questionInv: false, genreSujet: "F")
         #expect(forme == expectedForms[temps - 1][personne - 1])
         } */
    }
    //"se payer#15"
    @Test("Conjuguer s'équiper doit donner les bonnes formes")
    func conjuguerSEquiperDoitDonnerLesBonnesFormes() async throws {
        let verbe = "s'équiper#1"
        let genre = "H"
        let expectedForms = [
            ["m'équipais", "t'équipais", "s'équipait", "nous équipions", "vous équipiez", "s'équipaient"],
            ["m'équipe", "t'équipes", "s'équipe", "nous équipons", "vous équipez", "s'équipent"],
            ["m'équiperai", "t'équiperas", "s'équipera", "nous équiperons", "vous équiperez", "s'équiperont"],
            ["équipé", "s'équipant"]
        ]
        for temps in 1...3 {
            for personne in 1...6 {
                let forme = generator.conjuguer(verbe, temps: temps, pers: [personne], questionInv: false, genreSujet: genre)
                #expect(forme == expectedForms[temps - 1][personne - 1])
            }
        }
    }
    
    @Test("Conjuguer vouvoyer doit donner les bonnes formes")
    func conjuguerVouvoyerDoitDonnerLesBonnesFormes() async throws {
        let verbe = "vouvoyer#15"
        let genre = "H"
        let expectedForms = [
            ["vouvoyais", "vouvoyais", "vouvoyait", "vouvoyions", "vouvoyiez", "vouvoyaient"],
            ["vouvoie", "vouvoies", "vouvoie", "vouvoyons", "vouvoyez", "vouvoient"],
            ["vouvoierai", "vouvoieras", "vouvoiera", "vouvoierons", "vouvoierez", "vouvoieront"],
            ["vouvoyé", "vouvoyant"]
        ]
        for temps in 1...3 {
            for personne in 1...6 {
                let forme = generator.conjuguer(verbe, temps: temps, pers: [personne], questionInv: false, genreSujet: genre)
                #expect(forme == expectedForms[temps - 1][personne - 1])
            }
        }
    }
    
    @Test("Conjuguer vouvoyer mode interrogatif doit donner les bonnes formes")
    func conjuguerVouvoyerModeInterrogatifDoitDonnerLesBonnesFormes() async throws {
        let verbe = "vouvoyer#15"
        let genre = "H"
        let expectedForms = [
            ["vouvoyais-je", "vouvoyais-tu", "vouvoyait-il", "vouvoyions-nous", "vouvoyiez-vous", "vouvoyaient-ils"],
            ["vouvoie-je", "vouvoies-tu", "vouvoie-t-il", "vouvoyons-nous", "vouvoyez-vous", "vouvoient-ils"],
            ["vouvoierai-je", "vouvoieras-tu", "vouvoiera-t-il", "vouvoierons-nous", "vouvoierez-vous", "vouvoieront-ils"],
            ["vouvoyé", "vouvoyant"]
        ]
        for temps in 1...3 {
            for personne in 1...6 {
                let forme = generator.conjuguer(verbe, temps: temps, pers: [personne], questionInv: true, genreSujet: genre)
                #expect(forme == expectedForms[temps - 1][personne - 1])
            }
        }
    }
    
    @Test("Conjuguer vouvoyer mode interrogatif genre féminin doit donner les bonnes formes")
    func conjuguerVouvoyerModeInterrogatifGenreFemininDoitDonnerLesBonnesFormes() async throws {
        let verbe = "vouvoyer#15"
        let genre = "F"
        let expectedForms = [
            ["vouvoyais-je", "vouvoyais-tu", "vouvoyait-elle", "vouvoyions-nous", "vouvoyiez-vous", "vouvoyaient-elles"],
            ["vouvoie-je", "vouvoies-tu", "vouvoie-t-elle", "vouvoyons-nous", "vouvoyez-vous", "vouvoient-elles"],
            ["vouvoierai-je", "vouvoieras-tu", "vouvoiera-t-elle", "vouvoierons-nous", "vouvoierez-vous", "vouvoieront-elles"],
            ["vouvoyé", "vouvoyant"]
        ]
        for temps in 1...3 {
            for personne in 1...6 {
                let forme = generator.conjuguer(verbe, temps: temps, pers: [personne], questionInv: true, genreSujet: genre)
                #expect(forme == expectedForms[temps - 1][personne - 1])
            }
        }
    }
    
    @Test("Conjuguer offrir doit donner les bonnes formes")
    func conjuguerOffrirDoitDonnerLesBonnesFormes() async throws {
        let verbe = "offrir#16"
        let genre = "H"
        let expectedForms = [
            ["offrais", "offrais", "offrait", "offrions", "offriez", "offraient"],
            ["offre", "offres", "offre", "offrons", "offrez", "offrent"],
            ["offrirai", "offriras", "offrira", "offrirons", "offrirez", "offriront"],
            ["vouvoyé", "vouvoyant"]
        ]
        for temps in 1...3 {
            for personne in 1...6 {
                let forme = generator.conjuguer(verbe, temps: temps, pers: [personne], questionInv: false, genreSujet: genre)
                #expect(forme == expectedForms[temps - 1][personne - 1])
            }
        }
    }
    
    @Test("Conjuguer offrir mode interrogatif doit donner les bonnes formes")
    func conjuguerOffrirDoitModeInterrogatifDonnerLesBonnesFormes() async throws {
        let verbe = "offrir#16"
        let genre = "H"
        let modeInterrogatif = true
        let expectedForms = [
            ["offrais-je", "offrais-tu", "offrait-il", "offrions-nous", "offriez-vous", "offraient-ils"],
            ["offré-je", "offres-tu", "offre-t-il", "offrons-nous", "offrez-vous", "offrent-ils"],
            ["offrirai-je", "offriras-tu", "offrira-t-il", "offrirons-nous", "offrirez-vous", "offriront-ils"],
            ["vouvoyé", "vouvoyant"]
        ]
        for temps in 1...3 {
            for personne in 1...6 {
                let forme = generator.conjuguer(verbe, temps: temps, pers: [personne], questionInv: modeInterrogatif, genreSujet: genre)
                #expect(forme == expectedForms[temps - 1][personne - 1])
            }
        }
    }

    @Test("voyelle of empty string should be false")
    func beginWithVowelOfEmptyStringShouldBeFalse() async throws {
        #expect(generator.beginWithVowel("") == false)
    }
    
    @Test("voyelle of string beginning with a vowel should be true", arguments: ["a", "e", "i", "o", "u", "y", "h", "à", "â", "ä", "é", "è", "ê", "ë", "î", "ï", "ô", "ö", "ù", "û", "ü", "œ"])
    func beginWithVowelOfStringBeginningWithAVowelShouldBeTrue(_ input: String) async throws {
        let suffix = "qwertyuiop"
        var text = input.lowercased() + suffix
        #expect(generator.beginWithVowel(text) == true)
        text = input.uppercased() + suffix
        #expect(generator.beginWithVowel(text) == true)
    }
    
    @Test("voyelle of string beginning with a consonant should be false")
    func beginWithVowelOfStringBeginningWithAConsonantShouldBeFalse() async throws {
        let text = "qwertyuiop"
        #expect(generator.beginWithVowel(text) == false)
    }
    
    // Test for the presence of a verb beginning with an aspirated h.
    // In this case, the elision algorithm should be improved.
    @Test("verb begining with an aspirated h", arguments: ["hâbler", "hacher", "hachurer", "haïr", "haler", "hâler", "haleter", "hancher", "handicaper", "hannetonner", "hanter", "happer", "haranguer", "harasser", "harceler", "harder", "harnacher", "harpailler", "harponner", "hasarder", "hâter", "haubaner", "hausser", "haver", "havir", "héler", "hennir", "hérisser", "herser", "heurter", "hiérarchiser", "hisser", "hocher", "hongrer", "hongroyer", "honnir", "hoqueter", "houblonner", "houer", "hourder", "houspiller", "housser", "hucher", "huer", "huir", "humer", "hurler"])
    func verbBeginingWithAspiratedH(_ input: String) async throws {
        for part in [ValeriGenerator.Part.verbeAvecPrepositionA, .verbeAvecPrepositionAvec2Obj, .verbeAvecPrepositionCodCoi, .verbeAvecPrepositionDe, .verbeAvecPrepositionEt2Obj, .verbeAvecPrepositionLieu, .verbeAvecPrepositionSur, .verbeIntransitif, .verbeModalSimple, .verbeModalSuivisDeA, .verbeModalSuivisDeDe, .verbeTransitif] {
            for verb in ValeriGenerator.data[part]! {
                #expect(verb.starts(with: input) == false, "In \(part), \(verb) starts with \(input)")
            }
        }
    }
    
    @Test("verify plural", arguments: zip(["circulaire", "vespéral²²vespéraux", "vespérale", "guidé²s par satellite"], ["circulaires", "vespéraux", "vespérales", "guidés par satellite"]))
    func verifyPlural(_ input: String, _ expected: String) async throws {
        #expect(generator.accordPluriel(mot: input, pluriel: true) == expected)
    }

    @Test("verify singular", arguments: zip(["circulaire", "vespéral²²vespéraux", "vespérale", "guidé²s par satellite"], ["circulaire", "vespéral", "vespérale", "guidé par satellite"]))
    func verifySingular(_ input: String, _ expected: String) async throws {
        #expect(generator.accordPluriel(mot: input, pluriel: false) == expected)
    }
    
    @Test("verify nombreEnLettres", arguments: zip([1, 17, 21, 100, 101, 1000, 1001, 1002, 1999, 2000],["un", "dix-sept", "vingt et un", "cent", "cent un","mille", "mille et un", "mille deux", "mille neuf cent quatre-vingt-dix-neuf", "deux mille"]))
    func verifyNombreEnLettres(_ input: Int, _ expected: String) async throws {
        #expect(generator.nombreEnLettres(input) == expected)
    }

}
