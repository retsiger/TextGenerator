//
//  StringGeneratorTests.swift
//
//
//  Created by Joël Brogniart on 26/03/2024.
//

import Testing
@testable import StringGenerator

@Suite("String generators tests")
struct StringGeneratorTests {
    
    @Test("Some generator should exists")
    func someGeneratorShouldExist() {
        #expect(String.availableGenerators.count != 0)
    }
    
    @Test("There must be the same number of string generators as generations")
    func stringGeneratorsCountShouldBeEqualToGenerationCount() {
        #expect(String.availableGenerators.count == String.Generator.allCases.count)
    }
    
    @Test("All string generations should have corresponding string generator")
    func allGeneratorsShouldHaveCorrespondingStringGenerator() {
        for generator in String.Generator.allCases {
            #expect(String.availableGenerators.contains(generator) == true, "There is no \(generator) string generator.")
        }
    }
    
    @Test("All generators should have a description")
    func allStringGeneratorsShouldHaveADescription() {
        for generator in String.availableGenerators {
            #expect(String.description(for: generator).isEmpty == false, "Description for text generator \(generator.rawValue) should not be empty.")
        }
    }

    @Test("All generator should generate non empty string")
    func allStringGeneratorsShouldGenerateNonEmptyString() {
        for generator in String.availableGenerators {
            #expect(String.generate(with: generator).isEmpty == false, "Generated text by text generator \(generator) should not be empty.")
        }
    }
}
