//
//  ValerieGeneratorStringTests.swift
//  StringGenerator
//
//  Created by Joël Brogniart on 03/12/2024.
//

import Testing

import Testing
@testable import StringGenerator

@Suite("ValeriGenerator string  tests")
struct ValerieGeneratorStringTests {

    @Test("replacingFirstOccurrence of string should replace at start")
    func replacingFirstOccurenceOfStringShouldReplaceAtStart() async throws {
        let text = "un deux trois quatre"
        let search = "un"
        let replacement = "one"
        let expected = "one deux trois quatre"
        #expect(text.replacingFirstOccurrence(of: search, with: replacement) == expected)
    }

    @Test("replacingFirstOccurrence of string should replace at end")
    func replacingFirstOccurenceOfStringShouldReplaceAtEnd() async throws {
        let text = "un deux trois quatre"
        let search = "quatre"
        let replacement = "four"
        let expected = "un deux trois four"
        #expect(text.replacingFirstOccurrence(of: search, with: replacement) == expected)
    }

    @Test("replacingFirstOccurrence of string should replace once only")
    func replacingFirstOccurenceOfStringShouldReplaceOnceOnly() async throws {
        let text = "un un un un"
        let search = "un"
        let replacement = "one"
        let expected = "one un un un"
        #expect(text.replacingFirstOccurrence(of: search, with: replacement) == expected)
    }

    @Test("replacingFirstOccurrence of regex should replace at start")
    func replacingFirstOccurenceOfRegexShouldReplaceAtStart() async throws {
        let text = "un deux trois quatre"
        let search = "[un]+"
        let replacement = "one"
        let expected = "one deux trois quatre"
        #expect(text.replacingFirstOccurrence(of: search, with: replacement, options: .regularExpression) == expected)
    }

    @Test("replacingFirstOccurrence of regex should replace at end")
    func replacingFirstOccurenceOfRegexShouldReplaceAtEnd() async throws {
        let text = "un deux trois quatre"
        let search = "q[^e]+e"
        let replacement = "four"
        let expected = "un deux trois four"
        #expect(text.replacingFirstOccurrence(of: search, with: replacement, options: .regularExpression) == expected)
    }

    @Test("replacingFirstOccurrence of regex should replace once only")
    func replacingFirstOccurenceOfRegexShouldReplaceOnceOnly() async throws {
        let text = "un un un un"
        let search = "[un]+"
        let replacement = "one"
        let expected = "one un un un"
        #expect(text.replacingFirstOccurrence(of: search, with: replacement, options: .regularExpression) == expected)
    }
    
    //replacingFirstOccurrence(of: "&(0){0,2}", with: text)
    @Test("replacingFirstOccurrence of regex with optional 0 should be replaced", arguments:
            zip(["un & deux & trois &0 quatre &00 cinq",
                 "un &0 deux & trois &0 quatre &00 cinq",
                 "un &00 deux & trois &0 quatre &00 cinq"],
                ["un puis deux & trois &0 quatre &00 cinq",
                 "un puis deux & trois &0 quatre &00 cinq",
                 "un puis deux & trois &0 quatre &00 cinq"]))
    func replacingFirstOccurrenceOfRegexWithOptionalZeroShouldBeReplaced(_ input: String, _ expected: String) async throws {
        let search = "&(0){0,2}"
        let replacement = "puis"
        #expect(input.replacingFirstOccurrence(of: search, with: replacement, options: .regularExpression) == expected)
    }
    
}
