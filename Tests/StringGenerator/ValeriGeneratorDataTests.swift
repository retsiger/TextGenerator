//
//  ValeriGeneratorDataTests.swift
//  StringGenerator
//
//  Created by Joël Brogniart on 26/11/2024.
//

import Testing
@testable import StringGenerator

@Suite("ValeriGenerator data tests")
struct ValeriGeneratorDataTests {
    let generator = ValeriGenerator()
    
    @Test("proba item should exist for some parts", arguments: [ValeriGenerator.Part.negation, .adjectifInterrogatif, .adjectifInterrogatifLieu, .adjectifInterrogatifManiere, .adjectifInterrogatifTemps, .ponctuationPointFinal, .gnPronomPers, .articleQuantifieur])
    func probaItemShouldExistForSomeParts(_ part: ValeriGenerator.Part) async throws {
        #expect(ValeriGenerator.proba[part] != nil, "grimoire.proba doesn't contain key \(part)")
    }
    
    @Test("data item should exist for each proba key")
    func dataItemShouldExistForEachProbaKey() async throws {
        let probaKeys = ValeriGenerator.proba.keys
        let dataKeys = ValeriGenerator.data.keys
        for key in probaKeys {
            #expect(dataKeys.contains(key), "grimoire.data doesn't contain key \(key)")
        }
    }
    
    @Test("data item and proba item should have the same count for corresponding part")
    func dataItemAndProbaItemShouldHaveTheSameCountForCorrespondingPart() async throws {
        let parts = ValeriGenerator.proba.keys
        for part in parts {
            #expect(ValeriGenerator.data[part] != nil, "grimoire.data doesn't contain key \(part)")
            #expect(ValeriGenerator.proba[part]!.count == ValeriGenerator.data[part]!.count, "grimoire.data and grimoire.proba don't have the same count for\(part)")
        }
    }
    
    @Test("Valid strings have corresponding code", arguments: ["AF", "AI", "AP", "CL", "CO", "CT", "GN", "NG", "P2", "PF", "PR_N", "PR_T", "PV", "QS", "ST", "VA", "VAV", "VD", "VET", "VG", "VM", "VN", "VOA", "VOD", "VOI", "VOS", "VT", "VTL"])
    func validStringsHaveCorrespondingCode(string: String) async throws {
        #expect(ValeriGenerator.Code(rawValue: string) != nil, "Code for \(string) not found.")
    }
    
    @Test("Non valid strings don't have corresponding code", arguments: ["unknown", "invalid"])
    func nonValidStringsDontHaveCorrespondingCode(string: String) async throws {
        #expect(ValeriGenerator.Code(rawValue: string) == nil, "Code for \(string) found")
    }
    
    @Test("Good code should have a corresponding part", arguments: [ValeriGenerator.Code.AF, .AI, .AP, .CL, .CO, .CT, .GN, .NG, .P2, .PF, .PR_N, .PR_T, .PV, .VA, .VAV, .VD, .VET, .VG, .VM, .VN, .VOA, .VOD, .VOI, .VOS, .VT, .VTL])
    func goodCodeShouldHaveACorrespondingPart(code: ValeriGenerator.Code) async throws {
        #expect(code.part() != nil, "Part for \(code.rawValue) not found")
    }
    
    @Test("Bad code should not have a corresponding part", arguments: [ValeriGenerator.Code.QS, .ST])
    func badCodeShouldNotHaveACorrespondingPart(code: ValeriGenerator.Code) async throws {
        #expect(code.part() == nil, "Part for \(code.rawValue) found")
    }
    
    @Test("Valid part should have corresponding data item")
    func validPartShouldHaveCorrespondingDataItem() async throws {
        let parts = ValeriGenerator.Part.allCases
        for part in parts {
            #expect(ValeriGenerator.data[part] != nil, "Data item for \(part) not found")
        }
    }

    func verifyDataWithItemsHasAsLeastTwoParts(for parts: [ValeriGenerator.Part], with separator: String) {
        for part in parts {
            for item in ValeriGenerator.data[part]! {
                if item.contains(separator) {
                    let subItems = item.split(separator: separator)
                    #expect(subItems.count >= 2, "In ValeriGenerator.data[.\(part)], \"\(item)\" with \(separator) should have two parts")
                }
            }
        }
    }
    
    @Test("gnPronomPers should end with @chiffre")
    func gnPronomPersShouldEndWithArobaseChiffre() async throws {
        let part = ValeriGenerator.Part.gnPronomPers
        for item in ValeriGenerator.data[part]! {
            var good = false
            if let _ = item.range(of: "@[1-6]$", options: .regularExpression) {
                good = true
            }
            #expect(good, "\"\(item)\" should end with @chiffre")
        }
    }
    
    @Test("gnPronomPers should have two parts separated by @")
    func gnPronomPersShouldHaveTwoPartsSeparatedByArobase() async throws {
        let part = ValeriGenerator.Part.gnPronomPers
        let separator = "@"
        for item in ValeriGenerator.data[part]! {
            let subItems = item.split(separator: separator)
            #expect(subItems.count == 2, "In ValeriGenerator.data[.\(part)], \"\(item)\" with \(separator) should have two parts")
            
        }
    }
        
    @Test("gnPronomPers with _ should have two parts")
    func gnPronomPersWithUnderscoreShouldHaveTwoParts() async throws {
        let parts: [ValeriGenerator.Part] = [.gnPronomPers]
        let separator = "_"
        verifyDataWithItemsHasAsLeastTwoParts(for: parts, with: separator)
    }
    
    @Test("NomCommun item should contains % or end with genre", arguments: [ValeriGenerator.Part.coNomCommun, .gnNomCommun])
    func nomCommunItemShouldContainsPercentOrEndWithGenre(part: ValeriGenerator.Part) async throws {
        let separator = "%"
        for item in ValeriGenerator.data[part]! {
            if item.contains(separator) {
                let bad = item.contains("_H") || item.contains("_F") || item.contains("_N")
                #expect(!bad, "In \"\(part)\", \"\(item)\" should not contain genre _H, _F or _N")
            } else {
                let good = item.hasSuffix("_H") || item.hasSuffix("_F") || item.hasSuffix("_N")
                #expect(good, "In \"\(part)\", \"\(item)\" should end with genre _H, _F or _N")
            }
        }
    }
    
    @Test("article with _ should have two parts")
    func articleWithUnderscoreShouldHaveTwoParts() async throws {
        let parts: [ValeriGenerator.Part] = [.articleDef, .articleIndef, .articleDemo, .articlePossS, .articlePossP, .articleQuantifieur]
        let separator = "_"
        verifyDataWithItemsHasAsLeastTwoParts(for: parts, with: separator)
    }
    
    @Test("gnComplementNomPost with % should have two parts")
    func gnComplementNomPostWithPercentShouldHaveTwoParts() async throws {
        let parts: [ValeriGenerator.Part] = [.gnComplementNomPost]
        let separator = "%"
        verifyDataWithItemsHasAsLeastTwoParts(for: parts, with: separator)
    }
    
    @Test("adjectif with _ should have two parts")
    func adjectifWithUnderscoreShouldHaveTwoParts() async throws {
        let parts: [ValeriGenerator.Part] = [.coAdjectifPre, .coAdjectifPost]
        let separator = "_"
        verifyDataWithItemsHasAsLeastTwoParts(for: parts, with: separator)
    }
    
    @Test("coAdjectifPre with ° should have two parts")
    func adjectifWithDegreeShouldHaveTwoParts() async throws {
        let parts: [ValeriGenerator.Part] = [.coAdjectifPre]
        let separator = "°"
        verifyDataWithItemsHasAsLeastTwoParts(for: parts, with: separator)
    }
    
    @Test("data parts with ²² plural should have two parts")
    func dataPartsWithDoubleTwoPluralShouldHaveTwoParts() async throws {
        let parts: [ValeriGenerator.Part] = [.gnNomCommun, .gnComplementNomPost, .coNomCommun, .coAdjectifPre, .coAdjectifPost]
        let separator = "²²"
        verifyDataWithItemsHasAsLeastTwoParts(for: parts, with: separator)
    }

    @Test("gnVarianteNP should contain @chiffre")
    func gnVarianteNPShouldContainArobaseChiffre() async throws {
        let part = ValeriGenerator.Part.gnVarianteNP
        for item in ValeriGenerator.data[part]! {
            var good = false
            if let _ = item.range(of: "@[3-6]", options: .regularExpression) {
                good = true
            }
            #expect(good, "gnVarianteNP \"\(item)\" should contain @chiffre to indicate person to use for conjugation.")
        }
    }

    @Test("data parts shouldn't have duplication")
    func dataPartsShouldNotHaveDuplication() async throws {
        let parts = ValeriGenerator.Part.allCases
        for part in parts {
            if part == .pronomReflexif {
                continue
            }
            for item in ValeriGenerator.data[part]! {
                var count = 0
                for otherItem in ValeriGenerator.data[part]! {
                    if otherItem == item {
                        count += 1
                    }
                }
                #expect(count == 1, "Duplicate \(item) found in \(part)")
            }
        }
    }
    
    @Test("complementObjDir should not contain gender or number indication (%, _ or ²)")
    func complementObjDirShouldNotContainGenderOrNumberIndication() async throws {
        let part = ValeriGenerator.Part.complementObjDir
        for item in ValeriGenerator.data[part]! {
            var good = true
            if let _ = item.range(of: "[_%²]", options: .regularExpression) {
                good = false
            }
            #expect(good, "complementObjDir \"\(item)\" should not contain gender or number indication (%, _ or ²).")
        }
    }
        
}
