//
//  HaddockGeneratorTests.swift
//  StringGenerator
//
//  Created by Joël Brogniart on 12/11/2024.
//

import Testing
@testable import StringGenerator

@Suite("Haddock generator tests")
struct HaddockGeneratorTests {
    let generator = HaddockGenerator()
    let expectedGenerator = String.Generator.haddock
    
    @Test("Generator name is correct")
    func generatorNameIsCorrect() {
        #expect(generator.name == expectedGenerator)
    }
    
    @Test("Generator generate should not return empty string")
    func generatorGenerateShouldNotReturnEmptyString() {
        #expect(generator.generate().isEmpty == false)
    }
    
    @Test("HaddockGenerator.addBougre should add Bougre")
    func haddockGeneratorAddBougreShouldAddBougre() {
        let insult = HaddockGenerator.vocabulary[0].base
        let insultWithBougre = generator.addBougre(insult)
        #expect(insultWithBougre.hasPrefix("bougre de "))
    }
    
    @Test("HaddockGenerator.addEspece should add Espece")
    func haddockGeneratorAddEspeceShouldAddEspece() {
        let insult = HaddockGenerator.vocabulary[0].base
        let insultWithEspece = generator.addEspece(insult)
        #expect(insultWithEspece.hasPrefix("espèce de "))
    }
    
    @Test("Last character of insult modified with HaddockGenerator.addPunctuation should be a ponctuation")
    func lastCharacterOfInsultModifiedWithHaddockGeneratorShouldBePunctuation() {
        let insult = HaddockGenerator.vocabulary[0].base
        let insultWithPunctuation = generator.addPunctuation(insult)
        #expect(insultWithPunctuation.last?.isPunctuation == true)
    }
    
    @Test("First character of insult modified by prepareInsult shoud be uppercased")
    func firstCharacterOfInsultModifiedByPrepareInsultShouldBeUppercased() {
        let insultData: HaddockGenerator.InsultData = ("insult", nil)
        let preparedInsult = generator.prepareInsult(insultData)
        #expect(preparedInsult.first?.isUppercase == true)
    }
    
    @Test("Preparation of insult with option b should sometime begin with Bougre d")
    func preparationOfInsultWithOptionBShouldSometimeBeginWithBougreD() {
        let insultData: HaddockGenerator.InsultData = ("insult", "b")
        var preparedInsult = ""
        var neverSeeBougre = true
        for _ in 1...20 {
            preparedInsult = generator.prepareInsult(insultData)
            if preparedInsult.hasPrefix("Bougre d") {
                neverSeeBougre = false
                break
            }
        }
        #expect(neverSeeBougre == false)
    }
    
    @Test("Preparation of insult with option e should sometime begin with Espèce d")
    func preparationOfInsultWithOptionEShouldSometimeBeginWithEspeceD() {
        let insultData: HaddockGenerator.InsultData = ("insult", "e")
        var preparedInsult = ""
        var neverSeeEspece = true
        for _ in 1...20 {
            preparedInsult = generator.prepareInsult(insultData)
            if preparedInsult.hasPrefix("Espèce d") {
                neverSeeEspece = false
                break
            }
        }
        #expect(neverSeeEspece == false)
    }
}
