# StringGenerator

Generate random strings.

## Overview

StringGenerator is a package that extend the `String` structure to add a set of generators to the structure. Each generator generate a random text.

## Usage

### String.availableGenerators: [String.Generator]

Getting the list of generators.

```swift
import StringGenerator

let generatorList = String.availableGenerators
```

### public func generate(with generator: String.Generator) -> String

Generate a new string with a specific generator.

```swift
import StringGenerator

let loremipsumText = String.generate(with: .loremipsum)
```


## Generators

| Generator          | Description of the result of the generation                          |
|:-------------------|:---------------------------------------------------------------------|
| `.haddock   `      | Generate insults Captain Haddock's way.                             |
|                    | https://fr.wikipedia.org/wiki/Vocabulaire_du_capitaine_Haddock       |
| `.loremipsum`      | Generate pseudo latin text.                                          |
|                    | https://en.wikipedia.org/wiki/Lorem_ipsum                            |
| `.newage`          | Generate a page of New Age poppycock.                                |
|                    | From the New Age Bullshit Generator of Seb Pearce.                   |
|                    | https://github.com/sebpearce/bullshit                                |
| `.pipotron`        | Generate a sentence the pipotron way (waffle, political language).   |
|                    | http://www.lepipotron.com                                            |
| `.valeri`          | Generate a French sentence, based on Romain Valeri’s Générateur.     |
|                    | http://romainvaleri.online.fr                                        |


## Example

```swift
import StringGenerator

/*
 For each generator, print the name of the generator and
 a text generated with it.
 */
for generator in String.availableGenerators {
    print("\(generator):\n\(String.generate(with: generator))")
}
```
