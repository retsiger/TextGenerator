//
//  PipotronGenerator.swift
//
//
//  Created by Joël Brogniart on 26/03/2024.
//

import Foundation
import NaturalLanguage

struct PipotronGenerator: Generator, LanguageSpecific {
    let name = String.Generator.pipotron
    
    func description() -> String {
        let description = String(localized:
"""
Generate a sentence the pipotron way (waffle, political language).
http://www.lepipotron.com
""", table: "Pipotron", bundle: .module, comment: "Pipotron generator description.")
        return description;
    }
    
    func generate() -> String {
        let part1 = Self.parts[0].randomElement()!
        let part2 = Self.parts[1].randomElement()!
        let part3 = Self.parts[2].randomElement()!
        let part4 = Self.parts[3].randomElement()!
        let part5 = Self.parts[4].randomElement()!
        let part6 = Self.parts[5].randomElement()!
        let part7 = Self.parts[6].randomElement()!
        let part8 = Self.parts[7].randomElement()!
        let text = String(localized: "\(part1) \(part2) \(part3), \(part4) \(part5) \(part6) \(part7) \(part8).", table: "Pipotron", bundle: .module)
        return cleanSentence(text)
    }
    
    static private let parts = [
        [
            String(localized: "avec", table: "Pipotron", bundle: .module),
            String(localized: "considérant", table: "Pipotron", bundle: .module),
            String(localized: "où que nous mène", table: "Pipotron", bundle: .module),
            String(localized: "eu égard à", table: "Pipotron", bundle: .module),
            String(localized: "vu", table: "Pipotron", bundle: .module),
            String(localized: "en ce qui concerne", table: "Pipotron", bundle: .module),
            String(localized: "dans le cas particulier de", table: "Pipotron", bundle: .module),
            String(localized: "quelle que soit", table: "Pipotron", bundle: .module),
            String(localized: "du fait de", table: "Pipotron", bundle: .module),
            String(localized: "tant que durera", table: "Pipotron", bundle: .module)
        ],
        [
            String(localized: "la situation", table: "Pipotron", bundle: .module),
            String(localized: "la conjoncture", table: "Pipotron", bundle: .module),
            String(localized: "la crise", table: "Pipotron", bundle: .module),
            String(localized: "l'inertie", table: "Pipotron", bundle: .module),
            String(localized: "l'impasse", table: "Pipotron", bundle: .module),
            String(localized: "l'extrémité", table: "Pipotron", bundle: .module),
            String(localized: "la dégradation des mœurs", table: "Pipotron", bundle: .module),
            String(localized: "la sinistrose", table: "Pipotron", bundle: .module),
            String(localized: "la dualité de la situation", table: "Pipotron", bundle: .module),
            String(localized: "la baisse de confiance", table: "Pipotron", bundle: .module)
        ],
        [
            String(localized: "présente", table: "Pipotron", bundle: .module),
            String(localized: "actuelle", table: "Pipotron", bundle: .module),
            String(localized: "qui nous occupe", table: "Pipotron", bundle: .module),
            String(localized: "qui est la nôtre", table: "Pipotron", bundle: .module),
            String(localized: "induite", table: "Pipotron", bundle: .module),
            String(localized: "conjoncturelle", table: "Pipotron", bundle: .module),
            String(localized: "contemporaine", table: "Pipotron", bundle: .module),
            String(localized: "de cette fin de siècle", table: "Pipotron", bundle: .module),
            String(localized: "de la société", table: "Pipotron", bundle: .module),
            String(localized: "de ces derniers temps", table: "Pipotron", bundle: .module)
        ],
        [
            String(localized: "il convient de", table: "Pipotron", bundle: .module),
            String(localized: "il faut", table: "Pipotron", bundle: .module),
            String(localized: "on se doit de", table: "Pipotron", bundle: .module),
            String(localized: "il est préférable de", table: "Pipotron", bundle: .module),
            String(localized: "il serait intéressant de", table: "Pipotron", bundle: .module),
            String(localized: "il ne faut pas négliger de", table: "Pipotron", bundle: .module),
            String(localized: "on ne peut se passer de", table: "Pipotron", bundle: .module),
            String(localized: "il est nécessaire de", table: "Pipotron", bundle: .module),
            String(localized: "il serait bon de", table: "Pipotron", bundle: .module),
            String(localized: "il faut de toute urgence", table: "Pipotron", bundle: .module)
        ],
        [
            String(localized: "étudier", table: "Pipotron", bundle: .module),
            String(localized: "examiner", table: "Pipotron", bundle: .module),
            String(localized: "ne pas négliger", table: "Pipotron", bundle: .module),
            String(localized: "prendre en considération", table: "Pipotron", bundle: .module),
            String(localized: "anticiper", table: "Pipotron", bundle: .module),
            String(localized: "imaginer", table: "Pipotron", bundle: .module),
            String(localized: "se préoccuper de", table: "Pipotron", bundle: .module),
            String(localized: "s'intéresser à", table: "Pipotron", bundle: .module),
            String(localized: "avoir à l'esprit", table: "Pipotron", bundle: .module),
            String(localized: "se remémorer", table: "Pipotron", bundle: .module)
        ],
        [
            String(localized: "toutes les", table: "Pipotron", bundle: .module),
            String(localized: "chacune des", table: "Pipotron", bundle: .module),
            String(localized: "la majorité des", table: "Pipotron", bundle: .module),
            String(localized: "toutes les", table: "Pipotron", bundle: .module),
            String(localized: "l'ensemble des", table: "Pipotron", bundle: .module),
            String(localized: "la somme des", table: "Pipotron", bundle: .module),
            String(localized: "la totalité des", table: "Pipotron", bundle: .module),
            String(localized: "la globalité des", table: "Pipotron", bundle: .module),
            String(localized: "toutes les", table: "Pipotron", bundle: .module),
            String(localized: "certaines", table: "Pipotron", bundle: .module)
        ],
        [
            String(localized: "solutions", table: "Pipotron", bundle: .module),
            String(localized: "issues", table: "Pipotron", bundle: .module),
            String(localized: "problématiques", table: "Pipotron", bundle: .module),
            String(localized: "voies", table: "Pipotron", bundle: .module),
            String(localized: "alternatives", table: "Pipotron", bundle: .module),
            String(localized: "solutions", table: "Pipotron", bundle: .module),
            String(localized: "issues", table: "Pipotron", bundle: .module),
            String(localized: "problématiques", table: "Pipotron", bundle: .module),
            String(localized: "voies", table: "Pipotron", bundle: .module),
            String(localized: "alternatives", table: "Pipotron", bundle: .module)
        ],
        [
            String(localized: "envisageables", table: "Pipotron", bundle: .module),
            String(localized: "possibles", table: "Pipotron", bundle: .module),
            String(localized: "déjà en notre possession", table: "Pipotron", bundle: .module),
            String(localized: "s'offrant à nous", table: "Pipotron", bundle: .module),
            String(localized: "de bon sens", table: "Pipotron", bundle: .module),
            String(localized: "envisageables", table: "Pipotron", bundle: .module),
            String(localized: "possibles", table: "Pipotron", bundle: .module),
            String(localized: "déjà en notre possession", table: "Pipotron", bundle: .module),
            String(localized: "s'offrant à nous", table: "Pipotron", bundle: .module),
            String(localized: "de bon sens", table: "Pipotron", bundle: .module)
        ]
    ]
}
