//
//  NewAgeGenerator.swift
//  
//
//  Created by Joël Brogniart on 09/04/2024.
//  Original code from Seb Pearce New Age Bullshit Generator
//  https://github.com/sebpearce/bullshit

import Foundation
import NaturalLanguage

struct NewAgeGenerator: Generator {
    let name = String.Generator.newage
    
    func description() -> String {
        let description = String(localized:
"""
Generate a page of New Age poppycock, from the New Age Bullshit Generator of Seb Pearce.
https://github.com/sebpearce/bullshit
""", table: "NewAge", bundle: .module, comment: "New Age generator description.")
        return description;
    }
    
    func generate() -> String {
        return generateText()
    }    
}

// Vocabulary
extension NewAgeGenerator: LanguageSpecific {
    func retrieveRandomWordOf(_ wordType: WordType) -> String {
        guard let result = Self.bullshitWords[wordType]?.randomElement() else {
            return ""
        }
        return result
    }
    
    func removePattern(topic: SentenceTopic, index: Int, in patternPool: inout SentencePatterns) {
        guard patternPool[topic]!.indices.contains(index) else {
            return
        }
        patternPool[topic]!.remove(at: index)
        if patternPool[topic]!.count == 0 {
            patternPool.removeValue(forKey: topic)
        }
    }
    
    func generateSentence(topic: SentenceTopic, with patternPool: inout SentencePatterns) -> String {
        guard let patternIndex = patternPool[topic]?.indices.randomElement() else {
            return ""
        }
        guard let pattern = patternPool[topic]?[patternIndex] else {
            return ""
        }
        removePattern(topic: topic, index: patternIndex, in: &patternPool)
        // Find ranges of text corresponding to known WordTypes
        var ranges: [Range<String.Index>] = []
        if #available(macOS 10.14, iOS 12.0, *) {
            let tokenizer = NLTokenizer(unit: .word)
            tokenizer.string = pattern
            tokenizer.enumerateTokens(in: pattern.startIndex..<pattern.endIndex) { range, _ in
                let word = String(pattern[range])
                if let _ = WordType(rawValue: word) {
                    ranges.append(range)
                }
                return true
            }
        } else {
            let range = NSRange(pattern.startIndex..., in: pattern)
            let regex = try! NSRegularExpression(pattern: "\\W?(\\w+)\\W?", options: [])
            let matches = regex.matches(in: pattern, options: [], range: range)
            matches.forEach { match in
                guard let subrange = Range(match.range(at: 1), in: pattern) else {
                    return
                }
                let word = String(pattern[subrange])
                if let _ = WordType(rawValue: word) {
                    ranges.append(subrange)
                }
            }
        }
        // Construct the sentence with the pattern and replacing wordTypes with a random one.
        var sentence = ""
        var last = pattern.startIndex
        for range in ranges {
            sentence += pattern[last ..< range.lowerBound]
            let wordType = WordType(rawValue: String(pattern[range]))!
            sentence += retrieveRandomWordOf(wordType)
            last = range.upperBound
        }
        sentence += pattern[last...]
        return cleanSentence(sentence)
    }
    
    func generateParagraph(numberOfSentences: Int, topic: SentenceTopic, with patternPool: inout SentencePatterns) -> String {
        var generated: [String] = []
        var topic = topic
        for _ in 0..<numberOfSentences {
            guard !patternPool.isEmpty else {
                break
            }
            // If topic is absent pick another topic
            if !patternPool.keys.contains(topic) {
                topic = patternPool.keys.randomElement()!
            }
            generated.append(generateSentence(topic: topic, with: &patternPool))
        }
        if generated.isEmpty {
            return ""
        }
        return generated.joined(separator: " ")
    }
    
    func generateText() -> String {
        var patternPool = Self.sentencePatterns
        var generated: [String] = []
        var currentTopic: SentenceTopic = .explaining
        // Main heading
        generated.append(generateSentence(topic: currentTopic, with: &patternPool))
        // Sub heading
        generated.append(generateParagraph(numberOfSentences: 2, topic: currentTopic, with: &patternPool))
        // Third heading
        if patternPool.count > 1 {
            currentTopic = patternPool.keys.randomElement()!
            while currentTopic != .history {
                currentTopic = patternPool.keys.randomElement()!
            }
        }
        generated.append(generateSentence(topic: currentTopic, with: &patternPool))
        // Three paragraphs
        for _ in 0..<3 {
            if !patternPool.isEmpty {
                currentTopic = patternPool.keys.randomElement()!
                generated.append(generateParagraph(numberOfSentences: 3, topic: currentTopic, with: &patternPool))
            }
        }
        // Quote
        if !patternPool.isEmpty {
            currentTopic = patternPool.keys.randomElement()!
            generated.append(generateSentence(topic: currentTopic, with: &patternPool))
        }
        // Paragraph
        if !patternPool.isEmpty {
            currentTopic = patternPool.keys.randomElement()!
            generated.append(generateParagraph(numberOfSentences: 3, topic: currentTopic, with: &patternPool))
        }
        return generated.joined(separator: "\n")
    }
    
    // To ease translation, hyphen at the end of adjPrefix have been removed.
    func deleteSpaceAfterHyphen(_ sentence: String) -> String {
        return sentence
    }
    
    // Following functions are not needed as the text is not transformed
    // in the same way as in the original code.
    func removeSpacesBeforePunctuation(_ sentence: String) -> String {
        return sentence
    }
    
    func addSpaceAfterQuestionMarks(_ sentence: String) -> String {
        return sentence
    }
    
    func insertSpaceBeforePunctuation(_ sentence: String) -> String {
        return sentence
    }
    
    func insertSpaceBetweenSentences(_ sentence: String) -> String {
        return sentence
    }
}
