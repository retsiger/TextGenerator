//
//  LoremipsumGenerator.swift
//
//
//  Created by Joël Brogniart on 10/04/2024.
//

import Foundation

struct LoremipsumGenerator: Generator {
    let name = String.Generator.loremipsum
    
    func description() -> String {
        let description = String(localized:
"""
Generate pseudo latin text.
https://en.wikipedia.org/wiki/Lorem_ipsum
""", table: "Loremipsum", bundle: .module, comment: "Lorem Ipsum generator description.")
        return description;
    }
    
    func generate() -> String {
        var text: [String] = []
        let numberOfParagraphs = Int.random(in: 3..<7)
        for _ in 0..<numberOfParagraphs {
            text.append(generateParagraph())
        }
        return text.joined(separator: "\n")
    }
    
    typealias LoremWords = Set<String>

    static private let loremWords: LoremWords = [
        "a",
        "ac",
        "adipiscing",
        "aenean",
        "aliquam",
        "aliquet",
        "amet",
        "ante",
        "arcu",
        "at",
        "augue",
        "bibendum",
        "blandit",
        "congue",
        "consectetuer",
        "consectetur",
        "consequat",
        "convallis",
        "cras",
        "cubilia",
        "curabitur",
        "curae",
        "cursus",
        "diam",
        "dignissim",
        "dolor",
        "dui",
        "duis",
        "egestas",
        "eleifend",
        "elementum",
        "elit",
        "enim",
        "erat",
        "eros",
        "est",
        "et",
        "eu",
        "euismod",
        "faucibus",
        "felis",
        "fermentum",
        "feugiat",
        "fusce",
        "gravida",
        "hendrerit",
        "iaculis",
        "id",
        "imperdiet",
        "in",
        "integer",
        "ipsum",
        "justo",
        "lacus",
        "lectus",
        "leo",
        "libero",
        "ligula",
        "lobortis",
        "lorem",
        "luctus",
        "maecenas",
        "magna",
        "massa",
        "mauris",
        "metus",
        "mi",
        "molestie",
        "mollis",
        "morbi",
        "nec",
        "nibh",
        "nisl",
        "non",
        "nonummy",
        "nulla",
        "nunc",
        "odio",
        "orci",
        "ornare",
        "pede",
        "pellentesque",
        "pharetra",
        "placerat",
        "porttitor",
        "posuere",
        "praesent",
        "pretium",
        "primis",
        "proin",
        "pulvinar",
        "purus",
        "quam",
        "quis",
        "rhoncus",
        "risus",
        "rutrum",
        "sapien",
        "scelerisque",
        "sed",
        "sem",
        "semper",
        "sit",
        "sodales",
        "sollicitudin",
        "suscipit",
        "suspendisse",
        "tellus",
        "tempor",
        "tempus",
        "tincidunt",
        "tortor",
        "tristique",
        "turpis",
        "ullamcorper",
        "ultrices",
        "ultricies",
        "ut",
        "varius",
        "vehicula",
        "vel",
        "velit",
        "vestibulum",
        "vitae",
        "vivamus",
        "volutpat",
        "vulputate"
    ]

    func selectWords(_ quantity: Int) -> LoremWords {
        if quantity >= Self.loremWords.count {
            return Self.loremWords
        }
        var selectedWords: LoremWords = []
        if quantity != 0 {
            while selectedWords.count < quantity {
                selectedWords.insert(Self.loremWords.randomElement()!)
            }
        }
        return selectedWords
    }

    func generateSentence() -> String {
        let words = selectWords(Int.random(in: 1..<12))
        return generateSentence(words)
    }

    func generateSentence(_ words: LoremWords) -> String {
        if words.count == 0 {
            return ""
        }
        var words = words
        var word = words.randomElement()!
        words.remove(word)
        var sentence = word.localizedCapitalized
        while words.count > 0 {
            if words.count > 1, Int.random(in: 0..<9) < 3 {
                sentence += ","
            }
            sentence += " "
            word = words.randomElement()!
            words.remove(word)
            sentence += word
        }
        sentence += "."
        return sentence
    }

    func generateParagraph() -> String {
        var paragraph: [String] = []
        let numberOfSentences = Int.random(in: 6..<15)
        for _ in 0..<numberOfSentences {
            paragraph.append(generateSentence())
        }
        return paragraph.joined(separator: " ")
    }
}
