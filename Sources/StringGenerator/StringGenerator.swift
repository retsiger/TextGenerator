//
//  StringGenerator.swift
//
//  Created by Joël Brogniart on 05/11/2024.
//
//  Abstract:
//  A String extension to generate random texts.
//

import Foundation

/// Allows a raw enum type to be compared by the underlying comparable RawValue
public protocol RawComparable : Comparable where Self : RawRepresentable, RawValue: Comparable {
}

extension RawComparable {
    public static func < (lhs: Self, rhs: Self) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}

/**
 A String extension that adds generators.
 */
extension String {
    /**
     Generators
     */
    public enum Generator: String, CaseIterable, RawComparable, Sendable {
        case haddock
        case loremipsum
        case newage
        case pipotron
        case valeri
    }
    
    public init(_ generator: Generator) {
        self = Self.generate(with: generator)
    }
    
    /**
     The set of available generators.
     */
    static let generators: Set = [
        AnyGenerator(HaddockGenerator()),
        AnyGenerator(LoremipsumGenerator()),
        AnyGenerator(NewAgeGenerator()),
        AnyGenerator(PipotronGenerator()),
        AnyGenerator(ValeriGenerator()),
    ]
    
    /**
     Give the list of available string generators.
     - Returns: An array with the available string generators sorted.
     */
    public static let availableGenerators: [String.Generator] = generators.allCases()
    
    /**
     Return the description of a generator.
     
     Put the description of a generator in a constant.
     ```swift
     let loremipsumGeneratorDescription = String.description(for: .loremipsum)
     ```
     
     - Parameter generator: A generator.
     - Returns: the description of the generator.
     */
    public static func description(for generator: String.Generator) -> String {
        return generators.description(for: generator)
    }
    
     /**
     Generate a random string.
     
     Use a generator to obtain a new string.
     ```swift
     import StringGenerator
     
     let text = String.generate(with: .pipotron)
     ```
     
     - Parameter generator: The generator that will be used.
     - Returns: A new string.
     */
    public static func generate(with generator: String.Generator) -> String {
        return generators.generate(with: generator)
    }
}
