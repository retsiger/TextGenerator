//
//  LanguageSpecific.swift
//
//
//  Created by Joël Brogniart on 29/05/2024.
//

import Foundation

protocol LanguageSpecific {
    func cleanSentence(_ sentence: String) -> String
    func capitalizeFirstLetter(_ sentence: String) -> String
    
    // English
    func replaceAWithAn(_ sentence: String) -> String
    
    // French
    func elision(_ sentence: String) -> String
    func contraction(_ sentence: String) -> String
}

extension LanguageSpecific {
    func cleanSentence(_ sentence: String) -> String {
        var sentence = sentence
        let currentLanguage =  Locale.current.language.languageCode?.identifier
        switch currentLanguage {
        case "fr":
            sentence = elision(sentence)
            sentence = contraction(sentence)
        default: // English
            sentence = replaceAWithAn(sentence)
        }
        sentence = capitalizeFirstLetter(sentence)
        return sentence
    }
    
    func capitalizeFirstLetter(_ sentence: String) -> String {
        guard !sentence.isEmpty else {
            return sentence
        }
        return sentence.prefix(1).localizedUppercase + sentence.suffix(sentence.count - 1)
    }
    
    // English
    func replaceAWithAn(_ sentence: String) -> String {
        // replace 'a [vowel]' with 'an [vowel]'
        // I added a \W before the [Aa] because one time I got
        // 'Dogman is the antithesis of knowledge' :)
        return sentence.replacingOccurrences(of: "(^|\\W)([Aa]) ([aeiou])", with: "$1$2n $3", options: [.regularExpression])
    }
    
    // French
    
    // When words "le" or "de" are followed by a word beginning with a vowel the "e" is replaced
    // by single quote (élision), ie. "le enfant" becomes "l'enfant".
    func elision(_ sentence: String) -> String {
        return sentence.replacingOccurrences(of: "(^|\\W)([LlDd])([Ee] )([aeiouéèê])", with: "$1$2'$4", options: [.regularExpression])
    }
    
    // "de le" is replaced by "du", "de les" is remplaced by "des",
    // "à le" is replaced by "au"; "à les" is replaced by "aux"
    func contraction(_ sentence: String) -> String {
        var sentence = sentence.replacingOccurrences(of: "(^|\\W)([Dd]e le)(\\W)", with: "$1du$3", options: [.regularExpression])
        sentence = sentence.replacingOccurrences(of: "(^|\\W)([Dd]e les)(\\W)", with: "$1des$3", options: [.regularExpression])
        sentence = sentence.replacingOccurrences(of: "(^|\\W)(à le)(\\W)", with: "$1au$3", options: [.regularExpression])
        sentence = sentence.replacingOccurrences(of: "(^|\\W)(à les)(\\W)", with: "$1aux$3", options: [.regularExpression])
        return sentence
    }
}
