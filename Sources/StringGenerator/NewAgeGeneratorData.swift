//
//  NewAgeGeneratorData.swift
//  StringGenerator
//
//  Created by Joël Brogniart on 11/12/2024.
//  Original code from Seb Pearce New Age Bullshit Generator
//  https://github.com/sebpearce/bullshit

import Foundation

extension NewAgeGenerator {
    enum SentenceTopic: String, CaseIterable {
        case explaining
        case warnings
        case future
        case you
        case history
    }
    
    typealias SentencePatterns = [SentenceTopic:[String]]
    
    static let sentencePatterns: SentencePatterns = [
        // explaining
        .explaining: [
            String(localized: "nMass is the driver of nMass.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "nMass is the nTheXOf of nMass, and of us.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "You and I are nPersonPlural of the nCosmos.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "We exist as fixedNP.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "We viPerson, we viPerson, we are reborn.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "Nothing is impossible.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "This life is nothing short of a ing nOf of adj nMass.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "Consciousness consists of fixedNP of quantum energy. “Quantum” means a ing of the adj.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "The goal of fixedNP is to plant the seeds of nMass rather than nMassBad.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "nMass is a constant.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "By ing, we viPerson.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "The nCosmos is adjWith fixedNP.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "To vTraverse the nPath is to become one with it.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "Today, science tells us that the essence of nature is nMass.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining"),
            String(localized: "nMass requires exploration.", table: "NewAgePattern", bundle: .module, comment: "pattern explaining")
        ],
        // warnings
        .warnings: [
            String(localized: "We can no longer afford to live with nMassBad.", table: "NewAgePattern", bundle: .module, comment: "pattern warnings"),
            String(localized: "Without nMass, one cannot viPerson.", table: "NewAgePattern", bundle: .module, comment: "pattern warnings"),
            String(localized: "Only a nPerson of the nCosmos may vtMass this nOf of nMass.", table: "NewAgePattern", bundle: .module, comment: "pattern warnings"),
            String(localized: "You must take a stand against nMassBad.", table: "NewAgePattern", bundle: .module, comment: "pattern warnings"),
            String(localized: "Yes, it is possible to vtDestroy the things that can vtDestroy us, but not without nMass on our side.", table: "NewAgePattern", bundle: .module, comment: "pattern warnings"),
            String(localized: "nMassBad is the antithesis of nMass.", table: "NewAgePattern", bundle: .module, comment: "pattern warnings"),
            String(localized: "You may be ruled by nMassBad without realizing it. Do not let it vtDestroy the nTheXOf of your nPath.", table: "NewAgePattern", bundle: .module, comment: "pattern warnings"),
            String(localized: "The complexity of the present time seems to demand a ing of our nOurPlural if we are going to survive.", table: "NewAgePattern", bundle: .module, comment: "pattern warnings"),
            String(localized: "nMassBad is born in the gap where nMass has been excluded.", table: "NewAgePattern", bundle: .module, comment: "pattern warnings"),
            String(localized: "Where there is nMassBad, nMass cannot thrive.", table: "NewAgePattern", bundle: .module, comment: "pattern warnings")
        ],
        // future hope
        .future: [
            String(localized: "Soon there will be a ing of nMass the likes of which the nCosmos has never seen.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "It is time to take nMass to the next level.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "Imagine a ing of what could be.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "Eons from now, we nPersonPlural will viPerson like never before as we are ppPerson by the nCosmos.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "It is a sign of things to come.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "The future will be a adj ing of nMass.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "This nPath never ends.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "We must learn how to lead adj lives in the face of nMassBad.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "We must vtPerson ourselves and vtPerson others.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "The nOf of nMass is now happening worldwide.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "We are being called to explore the nCosmos itself as an interface between nMass and nMass.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "It is in ing that we are ppPerson.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "The nCosmos is approaching a tipping point.", table: "NewAgePattern", bundle: .module, comment: "pattern future"),
            String(localized: "nameOfGod will vOpenUp adj nMass.", table: "NewAgePattern", bundle: .module, comment: "pattern future")
        ],
        // you and your problems
        .you: [
            String(localized: "Although you may not realize it, you are adj.", table: "NewAgePattern", bundle: .module, comment: "pattern you and your problèms"),
            String(localized: "nPerson, look within and vtPerson yourself.", table: "NewAgePattern", bundle: .module, comment: "pattern you and your problèms"),
            String(localized: "Have you found your nPath?", table: "NewAgePattern", bundle: .module, comment: "pattern you and your problèms"),
            String(localized: "How should you navigate this adj nCosmos?", table: "NewAgePattern", bundle: .module, comment: "pattern you and your problèms"),
            String(localized: "It can be difficult to know where to begin.", table: "NewAgePattern", bundle: .module, comment: "pattern you and your problèms"),
            String(localized: "If you have never experienced this nOf fixedAdvP, it can be difficult to viPerson.", table: "NewAgePattern", bundle: .module, comment: "pattern you and your problèms"),
            String(localized: "The nCosmos is calling to you via fixedNP. Can you hear it?", table: "NewAgePattern", bundle: .module, comment: "pattern you and your problèms")
        ],
        // history
        .history: [
            String(localized: "Throughout history, humans have been interacting with the nCosmos via fixedNP.", table: "NewAgePattern", bundle: .module, comment: "pattern history"),
            String(localized: "Reality has always been adjWith nPersonPlural whose nOurPlural are ppThingPrep nMass.", table: "NewAgePattern", bundle: .module, comment: "pattern history"),
            String(localized: "Our conversations with other nPersonPlural have led to a ing of adjPrefix adj consciousness.", table: "NewAgePattern", bundle: .module, comment: "pattern history"),
            String(localized: "Humankind has nothing to lose.", table: "NewAgePattern", bundle: .module, comment: "pattern history"),
            String(localized: "We are in the midst of a adj ing of nMass that will vOpenUp the nCosmos itself.", table: "NewAgePattern", bundle: .module, comment: "pattern history"),
            String(localized: "Who are we? Where on the great nPath will we be ppPerson?", table: "NewAgePattern", bundle: .module, comment: "pattern history"),
            String(localized: "We are at a crossroads of nMass and nMassBad.", table: "NewAgePattern", bundle: .module, comment: "pattern history")
        ]
    ]
    
    // Vocabulary
    enum WordType: String {
        case adj
        case adjBig
        case adjPrefix
        case adjProduct
        case adjWith
        case fixedAdvP
        case fixedAdvPPlace
        case fixedNP
        case ing
        case nameOfGod
        case nBenefits
        case nCosmos
        case nMass
        case nMassBad
        case nOf
        case nOurPlural
        case nPath
        case nPerson
        case nPersonPlural
        case nProduct
        case nSubject
        case nTheXOf
        case ppPerson
        case ppThingPrep
        case vOpenUp
        case vTraverse
        case viPerson
        case vtDestroy
        case vtMass
        case vtPerson
    }
    
    static let bullshitWords: [WordType:[String]] = [
        .nCosmos: [
            String(localized: "cosmos", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "quantum soup", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "infinite", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "universe", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "galaxy", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "multiverse", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "grid", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "quantum matrix", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "totality", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "quantum cycle", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "nexus", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "planet", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "solar system", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "world", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "stratosphere", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "dreamscape", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "biosphere", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos"),
            String(localized: "dreamtime", table: "NewAgeNCosmos", bundle: .module, comment: "nCosmos")
        ],
        .nPerson: [
            String(localized: "being", table: "NewAgeNPerson", bundle: .module, comment: "nPerson"),
            String(localized: "child", table: "NewAgeNPerson", bundle: .module, comment: "nPerson"),
            String(localized: "traveller", table: "NewAgeNPerson", bundle: .module, comment: "nPerson"),
            String(localized: "entity", table: "NewAgeNPerson", bundle: .module, comment: "nPerson"),
            String(localized: "lifeform", table: "NewAgeNPerson", bundle: .module, comment: "nPerson"),
            String(localized: "wanderer", table: "NewAgeNPerson", bundle: .module, comment: "nPerson"),
            String(localized: "visitor", table: "NewAgeNPerson", bundle: .module, comment: "nPerson"),
            String(localized: "prophet", table: "NewAgeNPerson", bundle: .module, comment: "nPerson"),
            String(localized: "seeker", table: "NewAgeNPerson", bundle: .module, comment: "nPerson"),
            String(localized: "Indigo Child", table: "NewAgeNPerson", bundle: .module, comment: "nPerson")
        ],
        .nPersonPlural: [
            String(localized: "beings", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "travellers", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "entities", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "lifeforms", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "dreamweavers", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "adventurers", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "pilgrims", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "warriors", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "messengers", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "dreamers", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "storytellers", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "seekers", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "spiritual brothers and sisters", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "mystics", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural"),
            String(localized: "starseeds", table: "NewAgeNPersonPlural", bundle: .module, comment: "nPersonPlural")
        ],
        .nMass: [
            String(localized: "consciousness", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "nature", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "beauty", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "knowledge", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "truth", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "life", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "healing", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "potential", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "freedom", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "purpose", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "coherence", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "choice", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "passion", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "understanding", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "balance", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "growth", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "inspiration", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "conscious living", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "energy", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "health", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "spacetime", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "learning", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "being", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "wisdom", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "stardust", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "sharing", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "science", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "curiosity", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "hope", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "wonder", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "faith", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "fulfillment", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "peace", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "rebirth", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "self-actualization", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "presence", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "power", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "will", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "flow", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "potentiality", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "chi", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "intuition", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "synchronicity", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "wellbeing", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "joy", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "love", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "karma", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "life-force", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "awareness", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "guidance", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "transformation", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "grace", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "divinity", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "non-locality", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "inseparability", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "interconnectedness", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "transcendence", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "empathy", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "insight", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "rejuvenation", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "ecstasy", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "aspiration", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "complexity", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "serenity", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "intention", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "gratitude", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "starfire", table: "NewAgeNMass", bundle: .module, comment: "nMass"),
            String(localized: "manna", table: "NewAgeNMass", bundle: .module, comment: "nMass")
        ],
        .nMassBad: [
            String(localized: "turbulence", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "pain", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "suffering", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "stagnation", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "desire", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "bondage", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "greed", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "selfishness", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "ego", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "dogma", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "illusion", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "delusion", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "yearning", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "discontinuity", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad"),
            String(localized: "materialism", table: "NewAgeNMassBad", bundle: .module, comment: "nMassBad")
        ],
        .nOurPlural: [
            String(localized: "souls", table: "NewAgeNOurPlural", bundle: .module, comment: "nOurPlural"),
            String(localized: "lives", table: "NewAgeNOurPlural", bundle: .module, comment: "nOurPlural"),
            String(localized: "dreams", table: "NewAgeNOurPlural", bundle: .module, comment: "nOurPlural"),
            String(localized: "hopes", table: "NewAgeNOurPlural", bundle: .module, comment: "nOurPlural"),
            String(localized: "bodies", table: "NewAgeNOurPlural", bundle: .module, comment: "nOurPlural"),
            String(localized: "hearts", table: "NewAgeNOurPlural", bundle: .module, comment: "nOurPlural"),
            String(localized: "brains", table: "NewAgeNOurPlural", bundle: .module, comment: "nOurPlural"),
            String(localized: "third eyes", table: "NewAgeNOurPlural", bundle: .module, comment: "nOurPlural"),
            String(localized: "essences", table: "NewAgeNOurPlural", bundle: .module, comment: "nOurPlural"),
            String(localized: "chakras", table: "NewAgeNOurPlural", bundle: .module, comment: "nOurPlural"),
            String(localized: "auras", table: "NewAgeNOurPlural", bundle: .module, comment: "nOurPlural")
        ],
        .nPath: [
            String(localized: "circuit", table: "NewAgeNPass", bundle: .module, comment: "nPath"),
            String(localized: "mission", table: "NewAgeNPass", bundle: .module, comment: "nPath"),
            String(localized: "journey", table: "NewAgeNPass", bundle: .module, comment: "nPath"),
            String(localized: "path", table: "NewAgeNPass", bundle: .module, comment: "nPath"),
            String(localized: "quest", table: "NewAgeNPass", bundle: .module, comment: "nPath"),
            String(localized: "vision quest", table: "NewAgeNPass", bundle: .module, comment: "nPath"),
            String(localized: "story", table: "NewAgeNPass", bundle: .module, comment: "nPath"),
            String(localized: "myth", table: "NewAgeNPass", bundle: .module, comment: "nPath")
        ],
        .nOf: [
            String(localized: "quantum leap", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "evolution", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "spark", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "lightning bolt", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "reintegration", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "vector", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "rebirth", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "revolution", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "wellspring", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "fount", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "source", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "fusion", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "canopy", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "flow", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "network", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "current", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "transmission", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "oasis", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "quantum shift", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "paradigm shift", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "metamorphosis", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "harmonizing", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "reimagining", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "rekindling", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "unifying", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "osmosis", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "vision", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "uprising", table: "NewAgeNOf", bundle: .module, comment: "nOf"),
            String(localized: "explosion", table: "NewAgeNOf", bundle: .module, comment: "nOf")
        ],
        .ing: [
            String(localized: "flowering", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "unfolding", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "blossoming", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "awakening", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "deepening", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "refining", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "maturing", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "evolving", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "summoning", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "unveiling", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "redefining", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "condensing", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "ennobling", table: "NewAgeIng", bundle: .module, comment: "ing"),
            String(localized: "invocation", table: "NewAgeIng", bundle: .module, comment: "ing")
        ],
        .adj: [
            String(localized: "enlightened", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "zero-point", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "quantum", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "high-frequency", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "Vedic", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "non-dual", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "conscious", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "sentient", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "sacred", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "infinite", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "primordial", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "ancient", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "powerful", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "spiritual", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "higher", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "advanced", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "internal", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "sublime", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "technological", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "dynamic", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "life-affirming", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "sensual", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "unrestricted", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "ever-present", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "endless", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "ethereal", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "astral", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "cosmic", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "spatial", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "transformative", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "unified", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "non-local", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "mystical", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "divine", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "self-aware", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "magical", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "amazing", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "interstellar", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "unlimited", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "authentic", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "angelic", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "karmic", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "psychic", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "pranic", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "consciousness-expanding", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "perennial", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "heroic", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "archetypal", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "mythic", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "intergalactic", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "holistic", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "joyous", table: "NewAgeAdj", bundle: .module, comment: "adj"),
            String(localized: "eternal", table: "NewAgeAdj", bundle: .module, comment: "adj")
        ],
        .adjBig: [
            String(localized: "epic", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "unimaginable", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "colossal", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "unfathomable", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "magnificent", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "enormous", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "jaw-dropping", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "ecstatic", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "powerful", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "untold", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "astonishing", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "incredible", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "breathtaking", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig"),
            String(localized: "staggering", table: "NewAgeAdjBig", bundle: .module, comment: "adjBig")
        ],
        .adjWith: [
            String(localized: "aglow with", table: "NewAgeAdjWith", bundle: .module, comment: "adjWith"),
            String(localized: "buzzing with", table: "NewAgeAdjWith", bundle: .module, comment: "adjWith"),
            String(localized: "beaming with", table: "NewAgeAdjWith", bundle: .module, comment: "adjWith"),
            String(localized: "full of", table: "NewAgeAdjWith", bundle: .module, comment: "adjWith"),
            String(localized: "overflowing with", table: "NewAgeAdjWith", bundle: .module, comment: "adjWith"),
            String(localized: "radiating", table: "NewAgeAdjWith", bundle: .module, comment: "adjWith"),
            String(localized: "bursting with", table: "NewAgeAdjWith", bundle: .module, comment: "adjWith"),
            String(localized: "electrified with", table: "NewAgeAdjWith", bundle: .module, comment: "adjWith")
        ],
        .adjPrefix: [
            String(localized: "ultra", table: "NewAgeAdjPrefix", bundle: .module, comment: "adjPrefix"),
            String(localized: "supra", table: "NewAgeAdjPrefix", bundle: .module, comment: "adjPrefix"),
            String(localized: "hyper", table: "NewAgeAdjPrefix", bundle: .module, comment: "adjPrefix"),
            String(localized: "pseudo", table: "NewAgeAdjPrefix", bundle: .module, comment: "adjPrefix")
        ],
        .vtMass: [
            String(localized: "inspire", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "integrate", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "ignite", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "discover", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "rediscover", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "foster", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "release", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "manifest", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "harmonize", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "engender", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "bring forth", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "bring about", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "create", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "spark", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "reveal", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "generate", table: "NewAgeVtMass", bundle: .module, comment: "vtMass"),
            String(localized: "leverage", table: "NewAgeVtMass", bundle: .module, comment: "vtMass")
        ],
        .vtPerson: [
            String(localized: "enlighten", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "inspire", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "empower", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "unify", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "strengthen", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "recreate", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "fulfill", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "change", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "develop", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "heal", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "awaken", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "synergize", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "ground", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "bless", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson"),
            String(localized: "beckon", table: "NewAgeVtPerson", bundle: .module, comment: "vtPerson")
        ],
        .viPerson: [
            String(localized: "exist", table: "NewAgeViPerson", bundle: .module, comment: "viPerson"),
            String(localized: "believe", table: "NewAgeViPerson", bundle: .module, comment: "viPerson"),
            String(localized: "grow", table: "NewAgeViPerson", bundle: .module, comment: "viPerson"),
            String(localized: "live", table: "NewAgeViPerson", bundle: .module, comment: "viPerson"),
            String(localized: "dream", table: "NewAgeViPerson", bundle: .module, comment: "viPerson"),
            String(localized: "reflect", table: "NewAgeViPerson", bundle: .module, comment: "viPerson"),
            String(localized: "heal", table: "NewAgeViPerson", bundle: .module, comment: "viPerson"),
            String(localized: "vibrate", table: "NewAgeViPerson", bundle: .module, comment: "viPerson"),
            String(localized: "self-actualize", table: "NewAgeViPerson", bundle: .module, comment: "viPerson")
        ],
        .vtDestroy: [
            String(localized: "destroy", table: "NewAgeVtDestroy", bundle: .module, comment: "vtDestroy"),
            String(localized: "eliminate", table: "NewAgeVtDestroy", bundle: .module, comment: "vtDestroy"),
            String(localized: "shatter", table: "NewAgeVtDestroy", bundle: .module, comment: "vtDestroy"),
            String(localized: "disrupt", table: "NewAgeVtDestroy", bundle: .module, comment: "vtDestroy"),
            String(localized: "sabotage", table: "NewAgeVtDestroy", bundle: .module, comment: "vtDestroy"),
            String(localized: "exterminate", table: "NewAgeVtDestroy", bundle: .module, comment: "vtDestroy"),
            String(localized: "obliterate", table: "NewAgeVtDestroy", bundle: .module, comment: "vtDestroy"),
            String(localized: "eradicate", table: "NewAgeVtDestroy", bundle: .module, comment: "vtDestroy"),
            String(localized: "extinguish", table: "NewAgeVtDestroy", bundle: .module, comment: "vtDestroy"),
            String(localized: "erase", table: "NewAgeVtDestroy", bundle: .module, comment: "vtDestroy"),
            String(localized: "confront", table: "NewAgeVtDestroy", bundle: .module, comment: "vtDestroy")
        ],
        .nTheXOf: [
            String(localized: "richness", table: "NewAgeNTheXOff", bundle: .module, comment: "nTheXOf"),
            String(localized: "truth", table: "NewAgeNTheXOff", bundle: .module, comment: "nTheXOf"),
            String(localized: "growth", table: "NewAgeNTheXOff", bundle: .module, comment: "nTheXOf"),
            String(localized: "nature", table: "NewAgeNTheXOff", bundle: .module, comment: "nTheXOf"),
            String(localized: "healing", table: "NewAgeNTheXOff", bundle: .module, comment: "nTheXOf"),
            String(localized: "knowledge", table: "NewAgeNTheXOff", bundle: .module, comment: "nTheXOf"),
            String(localized: "birth", table: "NewAgeNTheXOff", bundle: .module, comment: "nTheXOf"),
            String(localized: "deeper meaning", table: "NewAgeNTheXOff", bundle: .module, comment: "nTheXOf")
        ],
        .ppPerson: [
            String(localized: "awakened", table: "NewAgePpPerson", bundle: .module, comment: "ppPerson"),
            String(localized: "re-energized", table: "NewAgePpPerson", bundle: .module, comment: "ppPerson"),
            String(localized: "recreated", table: "NewAgePpPerson", bundle: .module, comment: "ppPerson"),
            String(localized: "reborn", table: "NewAgePpPerson", bundle: .module, comment: "ppPerson"),
            String(localized: "guided", table: "NewAgePpPerson", bundle: .module, comment: "ppPerson"),
            String(localized: "aligned", table: "NewAgePpPerson", bundle: .module, comment: "ppPerson")
        ],
        .ppThingPrep: [
            String(localized: "enveloped in", table: "NewAgePpThingPrep", bundle: .module, comment: "ppThingPrep"),
            String(localized: "transformed into", table: "NewAgePpThingPrep", bundle: .module, comment: "ppThingPrep"),
            String(localized: "nurtured by", table: "NewAgePpThingPrep", bundle: .module, comment: "ppThingPrep"),
            String(localized: "opened by", table: "NewAgePpThingPrep", bundle: .module, comment: "ppThingPrep"),
            String(localized: "immersed in", table: "NewAgePpThingPrep", bundle: .module, comment: "ppThingPrep"),
            String(localized: "engulfed in", table: "NewAgePpThingPrep", bundle: .module, comment: "ppThingPrep"),
            String(localized: "baptized in", table: "NewAgePpThingPrep", bundle: .module, comment: "ppThingPrep")
        ],
        .fixedAdvP: [
            String(localized: "through non-local interactions", table: "NewAgeFixedAdvP", bundle: .module, comment: "fixedAdvP"),
            String(localized: "inherent in nature", table: "NewAgeFixedAdvP", bundle: .module, comment: "fixedAdvP"),
            String(localized: "at the quantum level", table: "NewAgeFixedAdvP", bundle: .module, comment: "fixedAdvP"),
            String(localized: "at the speed of light", table: "NewAgeFixedAdvP", bundle: .module, comment: "fixedAdvP"),
            String(localized: "of unfathomable proportions", table: "NewAgeFixedAdvP", bundle: .module, comment: "fixedAdvP"),
            String(localized: "on a cosmic scale", table: "NewAgeFixedAdvP", bundle: .module, comment: "fixedAdvP"),
            String(localized: "devoid of self", table: "NewAgeFixedAdvP", bundle: .module, comment: "fixedAdvP"),
            String(localized: "of the creative act", table: "NewAgeFixedAdvP", bundle: .module, comment: "fixedAdvP")
        ],
        .fixedAdvPPlace: [
            String(localized: "in this dimension", table: "NewAgeFixedAdvPPlace", bundle: .module, comment: "fixedAdvPPlace"),
            String(localized: "outside time", table: "NewAgeFixedAdvPPlace", bundle: .module, comment: "fixedAdvPPlace"),
            String(localized: "within the Godhead", table: "NewAgeFixedAdvPPlace", bundle: .module, comment: "fixedAdvPPlace"),
            String(localized: "at home in the cosmos", table: "NewAgeFixedAdvPPlace", bundle: .module, comment: "fixedAdvPPlace")
        ],
        .fixedNP: [
            String(localized: "expanding wave functions", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "superpositions of possibilities", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "electromagnetic forces", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "electromagnetic resonance", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "molecular structures", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "atomic ionization", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "electrical impulses", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "a resonance cascade", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "bio-electricity", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "ultrasonic energy", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "sonar energy", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "vibrations", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "frequencies", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "four-dimensional superstructures", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "ultra-sentient particles", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "sub-atomic particles", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "chaos-driven reactions", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "supercharged electrons", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "supercharged waveforms", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "pulses", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "transmissions", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "morphogenetic fields", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "bio-feedback", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "meridians", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "morphic resonance", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP"),
            String(localized: "psionic wave oscillations", table: "NewAgeFixedNP", bundle: .module, comment: "fixedNP")
        ],
        .nSubject: [
            String(localized: "alternative medicine", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "astrology", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "tarot", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "crystal healing", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "the akashic record", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "feng shui", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "acupuncture", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "homeopathy", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "aromatherapy", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "ayurvedic medicine", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "faith healing", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "prayer", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "astral projection", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "Kabala", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "reiki", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "naturopathy", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "numerology", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "affirmations", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "the Law of Attraction", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "tantra", table: "NewAgeNSubject", bundle: .module, comment: "nSubject"),
            String(localized: "breathwork", table: "NewAgeNSubject", bundle: .module, comment: "nSubject")
        ],
        .vOpenUp: [
            // 'open up", table: "NewAgeOpenUp", bundle: .module, comment: "vOpenUp"),
            String(localized: "give us access to", table: "NewAgeVOpenUp", bundle: .module, comment: "vOpenUp"),
            String(localized: "enable us to access", table: "NewAgeVOpenUp", bundle: .module, comment: "vOpenUp"),
            String(localized: "remove the barriers to", table: "NewAgeVOpenUp", bundle: .module, comment: "vOpenUp"),
            String(localized: "clear a path toward", table: "NewAgeVOpenUp", bundle: .module, comment: "vOpenUp"),
            String(localized: "let us access", table: "NewAgeVOpenUp", bundle: .module, comment: "vOpenUp"),
            // 'tap into", table: "NewAgeOpenUp", bundle: .module, comment: "vOpenUp"),
            String(localized: "align us with", table: "NewAgeVOpenUp", bundle: .module, comment: "vOpenUp"),
            String(localized: "amplify our connection to", table: "NewAgeVOpenUp", bundle: .module, comment: "vOpenUp"),
            String(localized: "become our stepping-stone to", table: "NewAgeVOpenUp", bundle: .module, comment: "vOpenUp"),
            String(localized: "be a gateway to", table: "NewAgeVOpenUp", bundle: .module, comment: "vOpenUp")
        ],
        .vTraverse: [
            String(localized: "traverse", table: "NewAgeVTraverse", bundle: .module, comment: "vTraverse"),
            String(localized: "walk", table: "NewAgeVTraverse", bundle: .module, comment: "vTraverse"),
            String(localized: "follow", table: "NewAgeVTraverse", bundle: .module, comment: "vTraverse"),
            String(localized: "engage with", table: "NewAgeVTraverse", bundle: .module, comment: "vTraverse"),
            String(localized: "go along", table: "NewAgeVTraverse", bundle: .module, comment: "vTraverse"),
            String(localized: "roam", table: "NewAgeVTraverse", bundle: .module, comment: "vTraverse"),
            String(localized: "navigate", table: "NewAgeVTraverse", bundle: .module, comment: "vTraverse"),
            String(localized: "wander", table: "NewAgeVTraverse", bundle: .module, comment: "vTraverse"),
            String(localized: "embark on", table: "NewAgeVTraverse", bundle: .module, comment: "vTraverse")
        ],
        .nameOfGod: [
            String(localized: "Gaia", table: "NewAgeNameOfGod", bundle: .module, comment: "nameOfGod"),
            String(localized: "Shiva", table: "NewAgeNameOfGod", bundle: .module, comment: "nameOfGod"),
            String(localized: "Parvati", table: "NewAgeNameOfGod", bundle: .module, comment: "nameOfGod"),
            String(localized: "the Goddess", table: "NewAgeNameOfGod", bundle: .module, comment: "nameOfGod"),
            String(localized: "Shakti", table: "NewAgeNameOfGod", bundle: .module, comment: "nameOfGod")
        ],
        .nBenefits: [
            String(localized: "improved focus", table: "NewAgeNBenefits", bundle: .module, comment: "nBenefits"),
            String(localized: "improved concentration", table: "NewAgeNBenefits", bundle: .module, comment: "nBenefits"),
            String(localized: "extreme performance", table: "NewAgeNBenefits", bundle: .module, comment: "nBenefits"),
            String(localized: "enlightenment", table: "NewAgeNBenefits", bundle: .module, comment: "nBenefits"),
            String(localized: "cellular regeneration", table: "NewAgeNBenefits", bundle: .module, comment: "nBenefits"),
            String(localized: "an enhanced sexual drive", table: "NewAgeNBenefits", bundle: .module, comment: "nBenefits"),
            String(localized: "improved hydration", table: "NewAgeNBenefits", bundle: .module, comment: "nBenefits"),
            String(localized: "psychic rejuvenation", table: "NewAgeNBenefits", bundle: .module, comment: "nBenefits"),
            String(localized: "a healthier relationship with the Self", table: "NewAgeNBenefits", bundle: .module, comment: "nBenefits")
        ],
        .adjProduct: [
            String(localized: "alkaline", table: "NewAgeAdjProduct", bundle: .module, comment: "adjProduct"),
            String(localized: "quantum", table: "NewAgeAdjProduct", bundle: .module, comment: "adjProduct"),
            String(localized: "holographic", table: "NewAgeAdjProduct", bundle: .module, comment: "adjProduct"),
            String(localized: "zero-point energy", table: "NewAgeAdjProduct", bundle: .module, comment: "adjProduct"),
            String(localized: "“living”", table: "NewAgeAdjProduct", bundle: .module, comment: "adjProduct"),
            String(localized: "metaholistic", table: "NewAgeAdjProduct", bundle: .module, comment: "adjProduct"),
            String(localized: "ultraviolet", table: "NewAgeAdjProduct", bundle: .module, comment: "adjProduct"),
            String(localized: "ozonized", table: "NewAgeAdjProduct", bundle: .module, comment: "adjProduct"),
            String(localized: "ion-charged", table: "NewAgeAdjProduct", bundle: .module, comment: "adjProduct"),
            String(localized: "hexagonal-cell", table: "NewAgeAdjProduct", bundle: .module, comment: "adjProduct"),
            String(localized: "organic", table: "NewAgeAdjProduct", bundle: .module, comment: "adjProduct")
        ],
        .nProduct: [
            String(localized: "water", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "healing crystals", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "Tibetan singing bowls", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "malachite runes", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "meditation bracelets", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "healing wands", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "rose quartz", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "karma bracelets", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "henna tattoos", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "hemp garments", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "hemp seeds", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "tofu", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "massage oil", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "herbal incense", table: "NewAgeNProduct", bundle: .module, comment: "nProduct"),
            String(localized: "cheesecloth tunics", table: "NewAgeNProduct", bundle: .module, comment: "nProduct")
        ]
    ]
}
