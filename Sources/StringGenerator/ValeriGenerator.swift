//
//  ValeriGenerator.swift
//  StringGenerator
//
//  Original from Romain Valeri
//  http://romainvaleri.online.fr
//  https://bitbucket.org/romainvaleri/fr_sentence_gen
//
//  Swift version by Joël Brogniart on 01/12/2024.
//

import Foundation

struct ValeriGenerator: Generator {
    let name = String.Generator.valeri
    
    func description() -> String {
        let description = String(localized:
"""
Génère une phrase, basé sur le Générateur de phrases de Romain Valeri.
http://romainvaleri.online.fr
""", table: "Valeri", bundle: .module, comment: "Valeri generator description.")
        return description;
    }
    
    func generate() -> String {
        return genererParagraphe()
    }
}

extension ValeriGenerator {
    // functions that where common to Grimoire and Generateur
    func de(_ n: Int) -> Int {
        return Int.random(in: 1...n)
    }
    
    func deProgressif_1() -> Int {
        var nombre: Int
        let jetQuantite = de(100)
        if jetQuantite < 60 {
            nombre = de(4) + 1
        } else if jetQuantite < 90 {
            nombre = de(10) + de(10) + de(10)
        } else if jetQuantite < 97 {
            nombre = de(10) * 10
        } else if jetQuantite < 99 {
            nombre = de(100) * 100
        } else {
            nombre = de(1000) + 1
        }
        return nombre
    }
        
    func probaSwitch<T>(_ tabValeurs: [T], _ tabPoids: [Int]) -> T {
        if tabValeurs.count != tabPoids.count {
            fatalError("arguments de la fonction probaSwitch() mal formés : tabValeurs.count = \(tabValeurs.count) différent de tabPoids.count = \(tabPoids.count)")
        }
        let somme = Double(tabPoids.reduce(0, +))
        let echelle = 100 / somme
        let tabPoids = tabPoids.map { Double($0) * echelle }
        let tirage = Double.random(in: 0..<100)
        for i in 0..<tabValeurs.count {
            var seuil = 0.0
            for j in 0...i {
                seuil += tabPoids[j]
            }
            if tirage <= seuil {
                return tabValeurs[i]
            }
            if i == (tabValeurs.count - 1) {
                return tabValeurs[i]
            }
        }
        return tabValeurs.last!
    }
    
    func beginWithVowel(_ string: String) -> Bool {
        if string.count == 0 {
            return false
        }
        return "\(Self.voyelleListe)".contains(string.first!.lowercased())
    }
}

extension String {
    func replacingFirstOccurrence(of pattern: String, with replacement: String, options: String.CompareOptions = []) -> String {
        if let range = self.range(of: pattern, options: options) {
            let substring = String(self[range])
            let transformedSubstring = substring.replacingOccurrences(of: pattern, with: replacement, options: options)
            
            var transformed = self[..<range.lowerBound]
            transformed += transformedSubstring
            transformed += self[range.upperBound...]
            return String(transformed)
        }
        return self
    }

    func split(by length: Int, lengthAtEnd: Bool = false) -> [String] {
        return stride(from: 0, to: count, by: length).map {
            var offset: Int
            if lengthAtEnd {
                let modulo = count % length
                if modulo == 0 {
                    offset = 0
                } else {
                    offset = length - modulo
                }
            } else {
                offset = 0
            }
            var startOffset = $0 - offset
            let stopOffset = startOffset + length
            startOffset = max(startOffset, 0)
            let start = index(startIndex, offsetBy: startOffset)
            let end = index(startIndex, offsetBy: stopOffset, limitedBy: endIndex) ?? endIndex
            return String(self[start..<end])
        }
    }
    
    func character(after index: String.Index) -> String {
        if index < endIndex {
            let nextIndex = self.index(after: index)
            return String(self[nextIndex])
        }
        return ""
    }
    
    func lastIndex(of input: String) -> String.Index? {
        return self.range(of: input, options: .backwards)?.lowerBound
    }
}

extension ValeriGenerator {
    func nombreEnLettres(_ nombre: Int) -> String? {
        // Possible usage of Foundation's NumberFormatter.Style.spellOut here.
        // The code is simpler but Valeri's version is better (more French correct).
        // At least in the context of a sentence generator.
        /*
         let formatter = NumberFormatter()
         formatter.numberStyle = .spellOut
         formatter.locale = Locale(identifier: "fr_FR")
         return formatter.string(from: NSNumber(value: nombre))
         */

        if abs(nombre) > 999_999_999_999 {
            return nil
        }
        let negatif = nombre < 0
        let chaine = String(abs(nombre))
        if chaine == "1001" {
            return ((negatif) ? "moins ": "") + "mille et un"
        }
        // tableau de correspondance pour tout nombre compris entre 0 et 99
        // on accède par exemple à "42" par >>> nombresFr[4][2]
        let nombresFr = [
            ["zéro", "un", "deux", "trois", "quatre", "cinq", "six", "sept", "£uit", "neuf"],
            ["dix", "onze", "douze", "treize", "quatorze", "quinze", "seize", "dix-sept", "dix-huit", "dix-neuf"],
            ["vingt", "vingt et un", "vingt-deux", "vingt-trois", "vingt-quatre", "vingt-cinq", "vingt-six", "vingt-sept", "vingt-huit", "vingt-neuf"],
            ["trente", "trente et un", "trente-deux", "trente-trois", "trente-quatre", "trente-cinq", "trente-six", "trente-sept", "trente-huit", "trente-neuf"],
            ["quarante", "quarante et un", "quarante-deux", "quarante-trois", "quarante-quatre", "quarante-cinq", "quarante-six", "quarante-sept", "quarante-huit", "quarante-neuf"],
            ["cinquante", "cinquante et un", "cinquante-deux", "cinquante-trois", "cinquante-quatre", "cinquante-cinq", "cinquante-six", "cinquante-sept", "cinquante-huit", "cinquante-neuf"],
            ["soixante", "soixante et un", "soixante-deux", "soixante-trois", "soixante-quatre", "soixante-cinq", "soixante-six", "soixante-sept", "soixante-huit", "soixante-neuf"],
            ["soixante-dix", "soixante et onze", "soixante-douze", "soixante-treize", "soixante-quatorze", "soixante-quinze", "soixante-seize", "soixante-dix-sept", "soixante-dix-huit", "soixante-dix-neuf"],
            ["quatre-vingt", "quatre-vingt-un", "quatre-vingt-deux", "quatre-vingt-trois", "quatre-vingt-quatre", "quatre-vingt-cinq", "quatre-vingt-six", "quatre-vingt-sept", "quatre-vingt-huit", "quatre-vingt-neuf"],
            ["quatre-vingt-dix", "quatre-vingt-onze", "quatre-vingt-douze", "quatre-vingt-treize", "quatre-vingt-quatorze", "quatre-vingt-quinze", "quatre-vingt-seize", "quatre-vingt-dix-sept", "quatre-vingt-dix-huit", "quatre-vingt-dix-neuf"]
        ]
        let grandeursFr = [["cent", "mille", "million", "milliard"], ["cents", "mille", "millions", "milliards"]]
        let zero = nombresFr[0][0]
        // découpage de la chaine en blocs de 3 chiffres, en partant des unités
        // par exemple, pour le nombre courant 42150, on obtient après cette étape
        // le tableau suivant : tabBloc >>> ["42", "150"]
        let blocs = chaine.split(by: 3, lengthAtEnd: true)
        // boucle sur les blocs : découpage du bloc courant, calcul de la chaine
        // correspondante au bloc courant, stockage dans tabResultat
        var tabResultat: [String] = []
        for i in 0..<blocs.count {
            let bloc = blocs[i]
            var tabBlock: [Int] = []
            for chiffre in bloc {
                tabBlock.append(Int(String(chiffre)) ?? 0)
            }
            while tabBlock.count < 3 {
                tabBlock.insert(0, at: 0)
            }
            let sousResultat = nombresFr[tabBlock[1]][tabBlock[2]]
            if tabBlock[0] > 1 {
                let exception100 = tabBlock[0] > 1 && sousResultat == zero && (blocs.count - 1) == i
                let centaines = nombresFr[0][tabBlock[0]] + " " + grandeursFr[(exception100) ? 1 : 0][0]
                let centaineSuffix = sousResultat == zero ? "" : " " + sousResultat
                tabResultat.append(centaines + centaineSuffix)
            } else if tabBlock[0] == 1 {
                let centSuffix = sousResultat == zero ? "" : " " + sousResultat
                tabResultat.append(grandeursFr[0][0] + centSuffix)
            } else if sousResultat != zero {
                tabResultat.append(sousResultat)
            }
            if (blocs.count > (i + 1)) && (bloc != "000") {
                let singulierOuPluriel = tabResultat[i] == nombresFr[0][1] ? 0 : 1
                let grandeur = grandeursFr[singulierOuPluriel][blocs.count - i - 1]
                let index = (i == (blocs.count - 2) && bloc == "1") ? (tabResultat.count - 1) : tabResultat.count
                while tabResultat.count < index + 1 {
                    tabResultat.append("")
                }
                tabResultat[index] = grandeur
            }
            if bloc.hasSuffix("80") && blocs.count == (i + 1) {
                let index = tabResultat.count - 1
                let plural80 = tabResultat[index].replacingFirstOccurrence(of: "quatre-vingt", with: "quatre-vingts")
                tabResultat[index] = plural80
            }
        }
        let resultat = tabResultat.joined(separator: " ")
        if resultat.isEmpty {
            return nombresFr[0][0]
        }
        let prefix = (negatif ? "moins " : "")
        return prefix + resultat
    }
}

// Romain Valeri's Grimoire functions
extension ValeriGenerator {
    func genererStructure() -> [String] {
        var structure = [String]()
        var flagCT = false
        var flagCL = false
        var flagAP = false
        
        if de(100) < 7 {
            if de(100) < 50 {
                structure.append("CT")
                flagCT = true
            } else {
                structure.append("CL")
                flagCL = true
            }
            structure.append("VG")
        }
        structure.append("GN")
        
        if de(100) < 26 {
            let tirageModal = de(100)
            if tirageModal < 37 {
                structure.append("VM")
            } else if tirageModal < 69 {
                structure.append("VD")
                structure.append("§de")
            } else {
                structure.append("VA")
                structure.append("§à")
            }
            //flagModal = true
            if de(100) < 4 {
                structure.append("AP")
                flagAP = true
            }
        }
        
        let verbeNonMod = ["VT", "VN", "VOA", "VOD", "VOI", "VTL", "VAV", "VET", "VOS"]
        let probaVerbeNonMod = [5, 5, 3, 3, 2, 2, 1, 1, 2]
        var verbe: String
        repeat {
            verbe = probaSwitch(verbeNonMod, probaVerbeNonMod)
        } while ((flagCL) && (verbe == "VTL"))
        structure.append(verbe)
        
        var seuilAp = flagAP ? 2 : 5
        if de(100) < seuilAp {
            structure.append("AP")
            flagAP = true
        }
        
        switch verbe {
        case "VT":
            structure.append((de(4) > 1) ? "CO" : "GN")
        case "VTL":
            structure.append("CL")
            flagCL = true
        case "VOA":
            structure.append("§à")
            structure.append((de(4) > 1) ? "CO" : "GN")
        case "VOD":
            structure.append("§de")
            structure.append((de(4) > 1) ? "CO" : "GN")
        case "VOS":
            structure.append("§sur")
            structure.append((de(4) > 1) ? "CO" : "GN")
        case "VOI":
            structure.append((de(11) > 1) ? "CO" : "GN")
            structure.append("§à")
            structure.append((de(11) > 1) ? "GN" : "CO")
        case "VAV":
            structure.append((de(5) > 1) ? "CO" : "GN")
            structure.append("§avec")
            structure.append((de(5) > 1) ? "CO" : "GN")
        case "VET":
            structure.append((de(5) > 1) ? "CO" : "GN")
            structure.append("§et")
            structure.append((de(5) > 1) ? "CO" : "GN")
        default :
            break
        }
        
        if de(100) < 20 {
            let tirageVerbeSuite = de(100)
            if tirageVerbeSuite < 25 {
                structure.append("PR_N")
                seuilAp = flagAP ? 2 : 5
                if de(100) < seuilAp {
                    structure.append("AP")
                    flagAP = true
                }
            } else if tirageVerbeSuite < 50 {
                structure.append("PR_T")
                seuilAp = flagAP ? 2 : 5
                if de(100) < seuilAp {
                    structure.append("AP")
                    flagAP = true
                }
                structure.append((de(4) > 1) ? "CO" : "GN")
            } else {
                structure.append((tirageVerbeSuite < 75) ? "§sans" : "§pour")
                repeat {
                    verbe = probaSwitch(verbeNonMod, probaVerbeNonMod)
                } while flagCL && (verbe == "CTL")
                structure.append(verbe)
                
                seuilAp = flagAP ? 2 : 5
                if de(100) < seuilAp {
                    structure.append("AP")
                    flagAP = true
                }
                switch verbe {
                case "VT":
                    structure.append((de(4) > 1) ? "CO" : "GN")
                case "VTL":
                    structure.append("CL")
                    flagCL = true
                case "VOA":
                    structure.append("§à")
                    structure.append((de(4) > 1) ? "CO" : "GN")
                case "VOD":
                    structure.append("§de")
                    structure.append((de(4) > 1) ? "CO" : "GN")
                case "VOS":
                    structure.append("§sur")
                    structure.append((de(4) > 1) ? "CO" : "GN")
                case "VOI":
                    structure.append((de(11) > 1) ? "CO" : "GN")
                    structure.append("§à")
                    structure.append((de(11) > 1) ? "CO" : "GN")
                case "VAV":
                    structure.append((de(5) > 1) ? "CO" : "GN")
                    structure.append("§avec")
                    structure.append((de(5) > 1) ? "CO" : "GN")
                case "VET":
                    structure.append((de(5) > 1) ? "CO" : "GN")
                    structure.append("§et")
                    structure.append((de(5) > 1) ? "CO" : "GN")
                default:
                    break
                }
            }
        }
        
        if (de(100) < 12) {
            let optionsFin = ["CT", "CL", "AF"]
            let probasFin = [3,3,5]
            var fin: String
            repeat {
                fin = probaSwitch(optionsFin, probasFin)
            } while (flagCT && fin == "CT") || (flagCL && fin == "CL")
            structure.append(fin)
        }
        
        for i in 0..<structure.count {
            if structure[i] == "AP" {
                if structure[i - 1].first! == "§" {
                    structure[i] = structure[i - 1]
                    structure[i - 1] = "AP"
                }
            }
        }
        
        return structure
    }
    
    func recupererMot(code: ValeriGenerator.Code) -> String? {
        if (code == .CT) && (de(6) > 5) {
            return de(3) > 1 ? date() : annee()
        }
        if let part = code.part() {
            return puiser(part)
        }
        return nil
    }
    
    func puiser(_ part: Self.Part) -> String {
        if Self.data.contains(where: { $0.key == part }),
           let count = Self.data[part]?.count,
           count >= 1 {
            let index = Int.random(in: 0..<count)
            return Self.data[part]![index]
        }
        return "\(part) inconnu ou vide"
    }
    
    func date() -> String {
        let firstInterval = Date().timeIntervalSinceReferenceDate
        let first = TimeInterval.random(in: 0...firstInterval) * 11
        let second = Date().timeIntervalSinceReferenceDate * 8
        let date = Date(timeIntervalSinceReferenceDate: first - second)
        let tps = (date < Date()) ? 1: 3
        var format: Date.FormatStyle
        if Bool.random() == true {
            format = Date.FormatStyle
                .dateTime
                .day(.twoDigits)
                .weekday(.wide)
                .month(.wide)
                .year(.defaultDigits)
                .locale(Locale(identifier: "fr_FR"))
        } else {
            if #available(iOS 18, macOS 15, *) {
                format = Date.FormatStyle
                    .dateTime
                    .day(.defaultDigits)
                    .weekday(.omitted)
                    .month(.wide)
                    .year(.defaultDigits)
                    .locale(Locale(identifier: "fr_FR"))
            } else {
                // Fallback on earlier versions
                format = Date.FormatStyle
                    .dateTime
                    .day(.defaultDigits)
                    .month(.wide)
                    .year(.defaultDigits)
                    .locale(Locale(identifier: "fr_FR"))
            }
        }
        return "le \(date.formatted(format))¤\(tps)"
    }
    
    func annee() -> String {
        var debut = ""
        var fin = ""
        var annee = Int.random(in: 1...4000) - 1001
        if annee == 0 {
            annee = 3000
        }
        if annee < 300 {
            fin = ((annee < 0) ? " avant": " après") + " J.C."
        }
        if (annee < 1000) || (annee > 2100) {
            debut = "l'an "
        }
        return "en \(debut)\(abs(annee))\(fin)"
    }
    
    func conjuguer(_ verbe: String, temps: Int, pers: [Int], questionInv: Bool, genreSujet: String ) -> String {
        var verbe = verbe
        var formes: [[String]] = []
        var prefixe: String = ""
        let pronominal = verbe.contains(/^s(\'|e )/)
        if pronominal {
            verbe = verbe.replacingFirstOccurrence(of: "^s(\'|e )(.*)", with: "$2", options: .regularExpression)
        }
        
        var personne: Int
        var persParticipe = 0
        if pers.count == 2 {
            persParticipe = pers[1]
            personne = pers[0]
        } else {
            personne = pers[0]
        }
        
        if var posGroupe = verbe.firstIndex(of: "#") { // Groupes réguliers
            posGroupe = verbe.index(after: posGroupe)
            let groupe = Int(verbe[posGroupe...])
            var racine = ""
            var terminaisons: [[String]]
            var inter = ""
            switch groupe {
            case 1: // verbes #1 : (modèle: chanter)
                racine = String(verbe[..<verbe.lastIndex(of: "er")!])
                terminaisons = [
                    ["ais", "ais", "ait", "ions", "iez", "aient"],
                    ["e", "es", "e", "ons", "ez", "ent"],
                    ["erai", "eras", "era", "erons", "erez", "eront"],
                    ["é", "ant"]
                ]
            case 2: // verbes #2 : (modèle: finir)
                racine = String(verbe[..<verbe.lastIndex(of: "ir")!])
                terminaisons = [
                    ["issais", "issais", "issait", "issions", "issiez", "issaient"],
                    ["is", "is", "it", "issons", "issez", "issent"],
                    ["irai", "iras", "ira", "irons", "irez", "iront"],
                    ["i", "issant"]
                ]
            case 3: // verbes #3 : (modèle: sentir)
                racine = String(verbe[..<verbe.lastIndex(of: "tir")!])
                terminaisons = [
                    ["tais", "tais", "tait", "tions", "tiez", "taient"],
                    ["s", "s", "t", "tons", "tez", "tent"],
                    ["tirai", "tiras", "tira", "tirons", "tirez", "tiront"],
                    ["ti", "tant"]
                ]
            case 4: // verbes #4 : (modèle: vendre/répondre)
                racine = String(verbe[..<verbe.lastIndex(of: "re")!])
                terminaisons = [
                    ["ais", "ais", "ait", "ions", "iez", "aient"],
                    ["s", "s", "", "ons", "ez", "ent"],
                    ["rai", "ras", "ra", "rons", "rez", "ront"],
                    ["u", "ant"]
                ]
            case 5: // verbes #5 : (modèle: paraître)
                racine = String(verbe[..<verbe.lastIndex(of: "aître")!])
                terminaisons = [
                    ["aissais", "aissais", "aissait", "aissions", "aissiez", "aissaient"],
                    ["ais", "ais", "aît", "aissons", "aissez", "aissent"],
                    ["aîtrai", "aîtras", "aîtra", "aîtrons", "aîtrez", "aîtront"],
                    ["u", "aissant"]
                ]
            case 6: // verbes #6 : (modèle: construire)
                racine = String(verbe[..<verbe.lastIndex(of: "re")!])
                terminaisons = [
                    ["sais", "sais", "sait", "sions", "siez", "saient"],
                    ["s", "s", "t", "sons", "sez", "sent"],
                    ["rai", "ras", "ra", "rons", "rez", "ront"],
                    ["t", "sant"]
                ]
            case 7: // verbes #7 : (modèle: peindre/joindre/craindre)
                racine = String(verbe[..<verbe.lastIndex(of: "ndre")!])
                terminaisons = [
                    ["gnais", "gnais", "gnait", "gnions", "gniez", "gnaient"],
                    ["ns", "ns", "nt", "gnons", "gnez", "gnent"],
                    ["ndrai", "ndras", "ndra", "ndrons", "ndrez", "ndront"],
                    ["nt", "gnant"]
                ]
            case 8: // verbes #8 : (modèle: tenir)
                racine = String(verbe[..<verbe.lastIndex(of: "enir")!])
                terminaisons = [
                    ["enais", "enais", "enait", "enions", "eniez", "enaient"],
                    ["iens", "iens", "ient", "enons", "enez", "iennent"],
                    ["iendrai", "iendras", "iendra", "iendrons", "iendrez", "iendront"],
                    ["enu", "enant"]
                ]
            case 9: // verbes #9 : (modèle: placer)
                racine = String(verbe[..<verbe.lastIndex(of: "cer")!])
                terminaisons = [
                    ["çais", "çais", "çait", "cions", "ciez", "çaient"],
                    ["ce", "ces", "ce", "çons", "cez", "cent"],
                    ["cerai", "ceras", "cera", "cerons", "cerez", "ceront"],
                    ["cé", "çant"]
                ]
            case 10: // verbes #10 : (modèle: manger)
                racine = String(verbe[..<verbe.lastIndex(of: "er")!])
                terminaisons = [
                    ["eais", "eais", "eait", "ions", "iez", "eaient"],
                    ["e", "es", "e", "eons", "ez", "ent"],
                    ["erai", "eras", "era", "erons", "erez", "eront"],
                    ["é", "eant"]
                ]
            case 11: // verbes #11 : (modèle: récupérer/accéder)
                let posEAigu = verbe.lastIndex(of: "é")!
                racine = String(verbe[..<posEAigu])
                inter = verbe.replacingFirstOccurrence(of: "^(.*)é([^é]*)er#11$", with: "$2", options:.regularExpression)
                terminaisons = [
                    ["é_ais", "é_ais", "é_ait", "é_ions", "é_iez", "é_aient"],
                    ["è_e", "è_es", "è_e", "é_ons", "é_ez", "è_ent"],
                    ["é_erai", "é_eras", "é_era", "é_erons", "é_erez", "é_eront"],
                    ["é_é", "é_ant"]
                ]
            case 12: // verbes #12 : (modèle: mener/lever/peser)
                var posEFaible = verbe.lastIndex(of: "e")!
                posEFaible = verbe[..<posEFaible].lastIndex(of: "e")!
                racine = String(verbe[..<posEFaible])
                inter = verbe.replacingFirstOccurrence(of: "^(.*)e([^e]*)er#12$", with: "$2", options:.regularExpression)
                terminaisons = [
                    ["e_ais", "e_ais", "e_ait", "e_ions", "e_iez", "e_aient"],
                    ["è_e", "è_es", "è_e", "e_ons", "e_ez", "è_ent"],
                    ["è_erai", "è_eras", "è_era", "è_erons", "è_erez", "è_eront"],
                    ["e_é", "e_ant"]
                ]
            case 13: // verbes #13 : (modèle: prendre
                racine = String(verbe[..<verbe.lastIndex(of: "endre")!])
                terminaisons = [
                    ["enais", "enais", "enait", "enions", "eniez", "enaient"],
                    ["ends", "ends", "end", "enons", "enez", "ennent"],
                    ["endrai", "endras", "endra", "endrons", "endrez", "endront"],
                    ["is", "enant"]
                ]
            case 14: // verbes #14 : (modèle: mettre)
                racine = String(verbe[..<verbe.lastIndex(of: "ettre")!])
                terminaisons = [
                    ["ettais", "ettais", "ettait", "ettions", "ettiez", "ettaient"],
                    ["ets", "ets", "et", "ettons", "ettez", "ettent"],
                    ["ettrai", "ettras", "ettra", "ettrons", "ettrez", "ettront"],
                    ["is", "ettant"]
                ]
            case 15: // verbes #15 : (modèle: essuyer/employer)
                racine = String(verbe[..<verbe.lastIndex(of: "yer")!])
                terminaisons = [
                    ["yais", "yais", "yait", "yions", "yiez", "yaient"],
                    ["ie", "ies", "ie", "yons", "yez", "ient"],
                    ["ierai", "ieras", "iera", "ierons", "ierez", "ieront"],
                    ["yé", "yant"]
                ]
            case 16: // verbes #16 : (modèle: ouvrir)
                racine = String(verbe[..<verbe.lastIndex(of: "rir")!])
                terminaisons = [
                    ["rais", "rais", "rait", "rions", "riez", "raient"],
                    ["re", "res", "re", "rons", "rez", "rent"],
                    ["rirai", "riras", "rira", "rirons", "rirez", "riront"],
                    ["ert", "rant"]
                ]
            case 17: // verbes #17 : (modèle: battre)
                racine = String(verbe[..<verbe.lastIndex(of: "tre")!])
                terminaisons = [
                    ["tais", "tais", "tait", "tions", "tiez", "taient"],
                    ["s", "s", "", "tons", "tez", "tent"],
                    ["trai", "tras", "tra", "trons", "trez", "tront"],
                    ["tu", "tant"]
                ]
            case 18: // verbes #18 : (modèle: écrire)
                racine = String(verbe[..<verbe.lastIndex(of: "re")!])
                terminaisons = [
                    ["vais", "vais", "vait", "vions", "viez", "vaient"],
                    ["s", "s", "t", "vons", "vez", "vent"],
                    ["rai", "ras", "ra", "rons", "rez", "ront"],
                    ["t", "vant"]
                ]
            case 19: // verbes #19 : (modèle: servir)
                racine = String(verbe[..<verbe.lastIndex(of: "vir")!])
                terminaisons = [
                    ["vais", "vais", "vait", "vions", "viez", "vaient"],
                    ["s", "s", "t", "vons", "vez", "vent"],
                    ["virai", "viras", "vira", "virons", "virez", "viront"],
                    ["vi", "vant"]
                ]
            case 20: // verbes #20 : (modèle: percevoir)
                racine = String(verbe[..<verbe.lastIndex(of: "cevoir")!])
                terminaisons = [
                    ["cevais", "cevais", "cevait", "cevions", "ceviez", "cevaient"],
                    ["çois", "çois", "çoit", "cevons", "cevez", "çoivent"],
                    ["cevrai", "cevras", "cevra", "cevrons", "cevrez", "cevront"],
                    ["çu", "cevant"]
                ]
            case 21: // verbes #21 : (modèle: jeter)
                racine = String(verbe[..<verbe.lastIndex(of: "er")!])
                terminaisons = [
                    ["ais", "ais", "ait", "ions", "iez", "aient"],
                    ["te", "tes", "te", "ons", "ez", "tent"],
                    ["terai", "teras", "tera", "terons", "terez", "teront"],
                    ["é", "ant"]
                ]
            case 22: // verbes #22 : (modèle: vivre)
                racine = String(verbe[..<verbe.lastIndex(of: "ivre")!])
                terminaisons = [
                    ["ivais", "ivais", "ivait", "ivions", "iviez", "ivaient"],
                    ["is", "is", "it", "ivons", "ivez", "ivent"],
                    ["ivrai", "ivras", "ivra", "ivrons", "ivrez", "ivront"],
                    ["écu", "ivant"]
                ]
            case 23: // verbes #23 : (modèle: appeler)
                racine = String(verbe[..<verbe.lastIndex(of: "er")!])
                terminaisons = [
                    ["ais", "ais", "ait", "ions", "iez", "aient"],
                    ["le", "les", "le", "ons", "ez", "lent"],
                    ["lerai", "leras", "lera", "lerons", "lerez", "leront"],
                    ["é", "ant"]
                ]
            default: //  Should never go there
                racine = "Mauvaise racine"
                terminaisons = [
                    ["Mauvaise terminaison", "Mauvaise terminaison", "Mauvaise terminaison", "Mauvaise terminaison", "Mauvaise terminaison", "Mauvaise terminaison"],
                    ["Mauvaise terminaison", "Mauvaise terminaison", "Mauvaise terminaison", "Mauvaise terminaison", "Mauvaise terminaison", "Mauvaise terminaison"],
                    ["Mauvaise terminaison", "Mauvaise terminaison", "Mauvaise terminaison", "Mauvaise terminaison", "Mauvaise terminaison", "Mauvaise terminaison"],
                    ["Mauvaise terminaison", "Mauvaise terminaison"]
                    
                ]
            }
            var ligne: [String]
            var terminaison: String
            for t in 0..<4 {
                ligne = []
                let maxP = t < 3 ? 6 : 2
                for p in 0..<maxP {
                    terminaison = terminaisons[t][p]
                    if inter.count > 0 {
                        terminaison = terminaison.replacingOccurrences(of: "_", with: inter)
                    }
                    ligne.append(racine + terminaison)
                }
                formes.append(ligne)
            }
        } else { // conjugaisons irrégulières ou non encore catégorisées */
            
            switch verbe {
            case "être":
                formes = [
                    ["étais", "étais", "était", "étions", "étiez", "étaient"],
                    ["suis", "es", "est", "sommes", "êtes", "sont"],
                    ["serai", "seras", "sera", "serons", "serez", "seront"],
                    ["été", "étant"]
                ]
            case "avoir":
                formes = [
                    ["avais", "avais", "avait", "avions", "aviez", "avaient"],
                    ["ai", "as", "a", "avons", "avez", "ont"],
                    ["aurai", "auras", "aura", "aurons", "aurez", "auront"],
                    ["eu", "ayant"]
                ]
            case "aller":
                formes = [
                    ["allais", "allais", "allait", "allions", "alliez", "allaient"],
                    ["vais", "vas", "va", "allons", "allez", "vont"],
                    ["irai", "iras", "ira", "irons", "irez", "iront"],
                    ["allé", "allant"]
                ]
            case "devoir":
                formes = [
                    ["devais", "devais", "devait", "devions", "deviez", "devaient"],
                    ["dois", "dois", "doit", "devons", "devez", "doivent"],
                    ["devrai", "devras", "devra", "devrons", "devrez", "devront"],
                    ["du", "devant"]
                ]
            case "voir":
                formes = [
                    ["voyais", "voyais", "voyait", "voyions", "voyiez", "voyaient"],
                    ["vois", "vois", "voit", "voyons", "voyez", "voient"],
                    ["verrai", "verras", "verra", "verrons", "verrez", "verront"],
                    ["vu", "voyant"]
                ]
            case "savoir":
                formes = [
                    ["savais", "savais", "savait", "savions", "saviez", "savaient"],
                    ["sais", "sais", "sait", "savons", "savez", "savent"],
                    ["saurai", "sauras", "saura", "saurons", "saurez", "sauront"],
                    ["su", "sachant"]
                ]
            case "pouvoir":
                formes = [
                    ["pouvais", "pouvais", "pouvait", "pouvions", "pouviez", "pouvaient"],
                    ["peux", "peux", "peut", "pouvons", "pouvez", "peuvent"],
                    ["pourrai", "pourras", "pourra", "pourrons", "pourrez", "pourront"],
                    ["pu", "pouvant"]
                ]
            case "résoudre":
                formes = [
                    ["résolvais", "résolvais", "résolvait", "résolvions", "résolviez", "résolvaient"],
                    ["résous", "résous", "résout", "résolvons", "résolvez", "résolvent"],
                    ["résoudrai", "résoudras", "résoudra", "résoudrons", "résoudrez", "résoudront"],
                    ["résolu", "résolvant"]
                ]
            case "mordre":
                formes = [
                    ["mordais", "mordais", "mordait", "mordions", "mordiez", "mordaient"],
                    ["mords", "mords", "mord", "mordons", "mordez", "mordent"],
                    ["mordrai", "mordras", "mordra", "mordrons", "mordrez", "mordront"],
                    ["mordu", "mordant"]
                ]
            case "envoyer":
                formes = [
                    ["envoyais", "envoyais", "envoyait", "envoyions", "envoyiez", "envoyaient"],
                    ["envoie", "envoies", "envoie", "envoyons", "envoyez", "envoient"],
                    ["enverrai", "enverras", "enverra", "enverrons", "enverrez", "enverront"],
                    ["envoyé", "envoyant"]
                ]
            case "faire":
                formes = [
                    ["faisais", "faisais", "faisait", "faisions", "faisiez", "faisaient"],
                    ["fais", "fais", "fait", "faisons", "faites", "font"],
                    ["ferai", "feras", "fera", "ferons", "ferez", "feront"],
                    ["fait", "faisant"]
                ]
            case "vouloir":
                formes = [
                    ["voulais", "voulais", "voulait", "voulions", "vouliez", "voulaient"],
                    ["veux", "veux", "veut", "voulons", "voulez", "veulent"],
                    ["voudrai", "voudras", "voudra", "voudrons", "voudrez", "voudront"],
                    ["voulu", "voulant"]
                ]
            case "croire":
                formes = [
                    ["croyais", "croyais", "croyait", "croyions", "croyiez", "croyaient"],
                    ["crois", "crois", "croit", "croyons", "croyez", "croient"],
                    ["croirai", "croiras", "croira", "croirons", "croirez", "croiront"],
                    ["cru", "croyant"]
                ]
            case "rire":
                formes = [
                    ["riais", "riais", "riait", "riions", "riiez", "riaient"],
                    ["ris", "ris", "rit", "rions", "riez", "rient"],
                    ["rirai", "riras", "rira", "rirons", "rirez", "riront"],
                    ["ri", "riant"]
                ]
            case "lire":
                formes = [
                    ["lisais", "lisais", "lisait", "lisions", "lisiez", "lisaient"],
                    ["lis", "lis", "lit", "lisons", "lisez", "lisent"],
                    ["lirai", "liras", "lira", "lirons", "lirez", "liront"],
                    ["lu", "lisant"]
                ]
            case "dire":
                formes = [
                    ["disais", "disais", "disait", "disions", "disiez", "disaient"],
                    ["dis", "dis", "dit", "disons", "dites", "disent"],
                    ["dirai", "diras", "dira", "dirons", "direz", "diront"],
                    ["dit", "disant"]
                ]
            case "interdire":
                formes = [
                    ["interdisais", "interdisais", "interdisait", "interdisions", "interdisiez", "interdisaient"],
                    ["interdis", "interdis", "interdit", "interdisons", "interdisez", "interdisent"],
                    ["interdirai", "interdiras", "interdira", "interdirons", "interdirez", "interdiront"],
                    ["interdit", "interdisant"]
                ]
            case "suivre":
                formes = [
                    ["suivais", "suivais", "suivait", "suivions", "suiviez", "suivaient"],
                    ["suis", "suis", "suit", "suivons", "suivez", "suivent"],
                    ["suivrai", "suivras", "suivra", "suivrons", "suivrez", "suivront"],
                    ["suivi", "suivant"]
                ]
            case "perdre":
                formes = [
                    ["perdais", "perdais", "perdait", "perdions", "perdiez", "perdaient"],
                    ["perds", "perds", "perd", "perdons", "perdez", "perdent"],
                    ["perdrai", "perdras", "perdra", "perdrons", "perdrez", "perdront"],
                    ["perdu", "perdant"]
                ]
            case "dormir":
                formes = [
                    ["dormais", "dormais", "dormait", "dormions", "dormiez", "dormaient"],
                    ["dors", "dors", "dort", "dormons", "dormez", "dorment"],
                    ["dormirai", "dormiras", "dormira", "dormirons", "dormirez", "dormiront"],
                    ["dormi", "dormant"]
                ]
            case "courir":
                formes = [
                    ["courais", "courais", "courait", "courions", "couriez", "couraient"],
                    ["cours", "cours", "court", "courons", "courez", "courent"],
                    ["courrai", "courras", "courra", "courrons", "courrez", "courront"],
                    ["couru", "courant"]
                ]
            case "recourir":
                formes = [
                    ["recourais", "recourais", "recourait", "recourions", "recouriez", "recouraient"],
                    ["recours", "recours", "recourt", "recourons", "recourez", "recourent"],
                    ["recourrai", "recourras", "recourra", "recourrons", "recourrez", "recourront"],
                    ["recouru", "recourant"]
                ]
            case "mourir":
                formes = [
                    ["mourais", "mourais", "mourait", "mourions", "mouriez", "mouraient"],
                    ["meurs", "meurs", "meurt", "mourons", "mourez", "meurent"],
                    ["mourrai", "mourras", "mourra", "mourrons", "mourrez", "mourront"],
                    ["mort", "mourant"]
                ]
            case "plaire":
                formes = [
                    ["plaisais", "plaisais", "plaisait", "plaisions", "plaisiez", "plaisaient"],
                    ["plais", "plais", "plaît", "plaisons", "plaisez", "plaisent"],
                    ["plairai", "plairas", "plaira", "plairons", "plairez", "plairont"],
                    ["plu", "plaisant"]
                ]
            case "nuire":
                formes = [
                    ["nuisais", "nuisais", "nuisait", "nuisions", "nuisiez", "nuisaient"],
                    ["nuis", "nuis", "nuit", "nuisons", "nuisez", "nuisent"],
                    ["nuirai", "nuiras", "nuira", "nuirons", "nuirez", "nuiront"],
                    ["nui", "nuisant"]
                ]
            case "fuir":
                formes = [
                    ["fuyais", "fuyais", "fuyait", "fuyions", "fuyiez", "fuyaient"],
                    ["fuis", "fuis", "fuit", "fuyons", "fuyez", "fuient"],
                    ["fuirai", "fuiras", "fuira", "fuirons", "fuirez", "fuiront"],
                    ["fui", "fuyant"]
                ]
            case "enfuir":
                formes = [
                    ["enfuyais", "enfuyais", "enfuyait", "enfuyions", "enfuyiez", "enfuyaient"],
                    ["enfuis", "enfuis", "enfuit", "enfuyons", "enfuyez", "enfuient"],
                    ["enfuirai", "enfuiras", "enfuira", "enfuirons", "enfuirez", "enfuiront"],
                    ["enfui", "enfuyant"]
                ]
            case "£aïr":
                formes = [
                    ["£aïssais", "£aïssais", "£aïssait", "£aïssions", "£aïssiez", "£aïssaient"],
                    ["£ais", "£ais", "£ait", "£aïssons", "£aïssez", "£aïssent"],
                    ["£aïrai", "£aïras", "£aïra", "£aïrons", "£aïrez", "£aïront"],
                    ["£aï", "£aïssant"]
                ]
            default: // Should never go there
                formes = [
                    ["Mauvaise forme", "Mauvaise forme", "Mauvaise forme", "Mauvaise forme", "Mauvaise forme", "Mauvaise forme"],
                    ["Mauvaise forme", "Mauvaise forme", "Mauvaise forme", "Mauvaise forme", "Mauvaise forme", "Mauvaise forme"],
                    ["Mauvaise forme", "Mauvaise forme", "Mauvaise forme", "Mauvaise forme", "Mauvaise forme", "Mauvaise forme"],
                    ["Mauvaise forme", "Mauvaise forme"]
                ]
            }
        }
        
        var retour = formes[temps - 1][personne - 1]
        var abbrev = false
        if (pronominal) {
            let part = ValeriGenerator.Part.pronomReflexif
            let pronoms = Self.data[part]!
            prefixe = pronoms[persParticipe > 0 ? persParticipe - 1 : personne - 1] + " "
            if beginWithVowel(retour) && prefixe.contains("e") {
                abbrev = true
            }
        }
        
        retour = prefixe + retour
        if abbrev {
            retour = retour.replacingFirstOccurrence(of: "e ", with: "'")
        }
        
        if questionInv {
            let pronomPers = Self.data[.gnPronomPers]![personne - 1]
            var particule = pronomPers.split(separator: "@")[0]
            if particule.contains("_") {
                particule = particule.split(separator: "_")[(genreSujet == "F" ? 1 : 0)]
            }
            let t = (personne % 3 == 0 && retour.last != "t") ? "t-" : ""
            retour += "-" + t + particule
            retour = retour.replacingFirstOccurrence(of: "e-je", with: "é-je")
            retour = retour.replacingFirstOccurrence(of: "oié-je", with: "oie-je")
        }
        return retour
    }
}

// Romain Valeri's Generateur functions
extension ValeriGenerator {
    func groupeNominal(estObjet: Bool) -> String {
        if de(5) > 4 {
            if de(2) > 2 {
                var variante: String = ""
                var genreVariante: String = ""
                repeat {
                    variante = puiser(.gnVarianteNP)
                } while estObjet && variante.contains(/\ et (moi|toi|nous|vous)/)
                if variante.contains("__H") {
                    variante = variante.replacingFirstOccurrence(of: "__H", with: "")
                    genreVariante = "H"
                } else if variante.contains("__F") {
                    variante = variante.replacingFirstOccurrence(of: "__F", with: "")
                    genreVariante = "F"
                }
                var posDollar = variante.firstIndex(of: "$")
                while posDollar != nil && posDollar! < variante.index(before: variante.endIndex) {
                    let lettreSuivante = variante.character(after: posDollar!)
                    let chercheF = lettreSuivante == "F"
                    let chercheM = lettreSuivante == "M"
                    var nomP: String
                    var fem: Bool
                    repeat {
                        repeat {
                            nomP = puiser(.gnNomPropre)
                            fem = nomP.contains("_F")
                        } while (chercheM && fem) || (chercheF && !fem)
                        nomP = nomP.replacingFirstOccurrence(of: "_F", with: "")
                    } while variante.contains(nomP)
                    if genreVariante == "" && !fem {
                        genreVariante = "H"
                    }
                    variante = variante.replacingFirstOccurrence(of: "$[FM]?", with: nomP, options: .regularExpression)
                    posDollar = variante.firstIndex(of: "$")
                }
                if genreVariante != "H" {
                    variante += "__F"
                }
                return variante.replacingOccurrences(of: " (d|qu)e ([\(Self.voyelleListe)])", with: " $1'$2", options: [.regularExpression, .caseInsensitive])
            }
            let result = puiser(.gnNomPropre)
            return result.replacingFirstOccurrence(of: "_F", with: "__F") + "@3"
        }

        // --- pronoms personnels ---
        if !estObjet && de(10) > 7 {
            let pronomP = probaSwitch(Self.data[.gnPronomPers]!, Self.proba[.gnPronomPers]!)
            if !pronomP.contains("_") {
                return pronomP + "_PP"
            }
            let parts = pronomP.split(separator: "@")
            let personne = parts[1]
            var pronoms = parts[0].split(separator: "_")
            pronoms[1] += "__F"
            return pronoms[(de(2) - 1)] + "@" + personne + "_PP"
        }
        
        // --- noms communs ---
        var nom = puiser(.gnNomCommun)

        if nom.contains("%") {
            var parts = nom.split(separator: "%")
            if parts[1].count == 1 {
                parts[1] = String(parts[0]).replacingFirstOccurrence(of: "².*$", with: "", options: .regularExpression) + parts[1]
            }
            parts[0] += "_H"
            parts[1] += "_F"
            nom = String(Bool.random() ? parts[1] : parts[0])
        }
        let decoupe = nom.split(separator: "_")
        var feminin = false
        // *** vérifier que les split donnent bien deux parties
        // parce qu'il y a un index out of range pour if decoupe[1] ci dessous.
        // C'était parce que pour "membre²s du parti" il n'y a ni % ni _
        // et donc split ne retourne qu'un élément.
        // Modifié data pour avoir "membre²s du parti_N" à la place.
        if decoupe[1] == "N" && Bool.random() {
            feminin = true
        } else if decoupe[1] == "F" {
            feminin = true
        }
        nom = String(decoupe[0])
        var pluriel = Bool.random()
        let voyelle = beginWithVowel(nom)
        var article: String
        let jetArticle = de(100)
        if jetArticle < 38 { // article défini
            article = Self.data[.articleDef]![pluriel ? 1 : 0]
        } else if jetArticle < 78 { // article indéfini
            article = Self.data[.articleIndef]![pluriel ? 1 : 0]
        } else if jetArticle < 84 { // article démonstratif
            article = Self.data[.articleDemo]![pluriel ? 1 : 0]
        } else if jetArticle < 96 { // quantifieur
            article = probaSwitch(Self.data[.articleQuantifieur]!, Self.proba[.articleQuantifieur]!)
            pluriel = article.contains("µ") == false
            if article.contains("%%") {
                article = article.replacingOccurrences(of: "%%", with: nombreEnLettres(de(100)) ?? "fauxNombre")
            }
        } else { // nombre entier
            let nombre = deProgressif_1()
            article = nombreEnLettres(nombre) ?? "fauxNombre"
            if feminin {
                article = article.replacingFirstOccurrence(of: "un$", with: "une", options: .regularExpression)
            }
            pluriel = true
        }
        if article.contains("_") {
            article = String(article.split(separator: "_")[feminin ? 1 : 0])
        }
        let plurielNom = article.contains("µ") ? true : pluriel
        article = article.replacingFirstOccurrence(of: "µ", with: "")
        nom = accordPluriel(mot: nom, pluriel: plurielNom)
        var complement = ""
        if de(4) > 3 {
            complement = puiser(.gnComplementNomPost)
            if complement.contains("%") {
                var parts = complement.split(separator: "%")
                if parts[1].count == 1 {
                    parts[1] = parts[0] + parts[1]
                }
                complement = String(parts[(feminin) ? 1 : 0])
            }
            complement = " " + accordPluriel(mot: complement, pluriel: plurielNom)
            var nombre: String
            while complement.contains("&") {
                nombre = nombreEnLettres((de(100) + 1)) ?? "fauxNombre"
                if feminin && nombre.hasSuffix("un") {
                    nombre += "e"
                }
                complement = complement.replacingFirstOccurrence(of: "&", with: nombre)
            }
        }
        var groupeN = article + " " + nom + complement
        if voyelle {
            groupeN = groupeN.replacingFirstOccurrence(of: ".\\*", with: "'", options: .regularExpression)
            groupeN = groupeN.replacingFirstOccurrence(of: "¤", with: "t")
        }
        groupeN = groupeN.replacingOccurrences(of: "[*¤]", with: "", options: .regularExpression)
        var codePers = ""
        if !estObjet {
            codePers = "@" + ((pluriel) ? "6" : "3")
        }
        return groupeN + codePers + (feminin ? "__F" : "")
    }
    
    func complementObjet(personneSujet: Int) -> String {
        var cObj = ""
        var nom = puiser(.coNomCommun)
        var indef = false;
        var feminin = false
        if nom.contains("_") {
            var parts = nom.split(separator: "_")
            if parts[1] == "N" && de(2) > 1 {
                parts[1] = "F"
            }
            feminin = parts[1] == "F"
            nom = String(parts[0])
        }
        var pluriel = de(2) == 1
        let voyelle = beginWithVowel(nom)
        
        var adjectif = ""
        var prePose = false
        let jetAdj = de(100)
        if (jetAdj < 20) {
            //adjectif = Generateur.CO.adjectifsPost.puiser();
            adjectif = puiser(.coAdjectifPost)
        } else if (jetAdj < 24) {
            //adjectif = Generateur.CO.adjectifsPre.puiser();
            adjectif = puiser(.coAdjectifPre)
          prePose = true;
        }
        let noAdj = adjectif.count == 0;
        
        var nombre = ""
        while adjectif.contains("&") {
            let n = deProgressif_1()
            nombre = nombreEnLettres(n)!
            adjectif = adjectif.replacingFirstOccurrence(of: "&", with: nombre)
        }
        
        if !noAdj && adjectif.contains("%") {
            var parts = adjectif.split(separator: "%")
            if parts[1].count == 1 {
                parts[1] = parts[0] + parts[1]
            }
            adjectif = String(parts[(feminin) ? 1 : 0])
        }
        
        var voyelleAdj = false;
        if !noAdj {
          voyelleAdj = beginWithVowel(adjectif)
        }
        
        var article = ""
        let jetArticle = de(100)
        if jetArticle < 35  { // définis
            article = Self.data[.articleDef]![pluriel ? 1 : 0]
        } else if jetArticle < 70 { // indéfinis
            article = Self.data[.articleIndef]![pluriel ? 1 : 0]
            indef = true;
        } else if jetArticle < 78 { // démonstratifs
            article = Self.data[.articleDemo]![pluriel ? 1 : 0]
        } else if jetArticle < 90  { // quantifieurs
            article = probaSwitch(Self.data[.articleQuantifieur]!, Self.proba[.articleQuantifieur]!)
            article = article.replacingFirstOccurrence(of: "µ", with: "")
            pluriel = true
            if article.contains("%%") {
                article = article.replacingFirstOccurrence(of: "%%", with: nombreEnLettres(de(100))!)
            }
        } else if jetArticle < 94  { // nombres entiers
            let nombre = deProgressif_1()
            article = nombreEnLettres(nombre)!
            if feminin {
                article = article.replacingFirstOccurrence(of: "un$", with: "une", options: .regularExpression)
            }
            pluriel = true
        } else { // possessifs
            var personne = 0
            while (personne == 0) {
                personne = de(6)
                if abs(personneSujet - personne) == 3 {
                    personne = 0
                }
            }
            article = pluriel ? ValeriGenerator.data[.articlePossP]![personne - 1] : ValeriGenerator.data[.articlePossS]![personne - 1]
            if article.contains("_") {
                var articleFeminin = feminin
                if (voyelle && articleFeminin) || (prePose && voyelleAdj) {
                    if noAdj || (!noAdj && !prePose) || (prePose && voyelleAdj) {
                        articleFeminin = false
                    }
                }
                article = String(article.split(separator: "_")[articleFeminin ? 1 : 0])
            }
        }
        if article.contains("_") {
            article = String(article.split(separator: "_")[feminin ? 1 : 0])
        }
        
        nom = accordPluriel(mot: nom, pluriel: pluriel);
        adjectif = accordPluriel(mot: adjectif, pluriel: pluriel);
        
        if prePose && adjectif.contains("°") {
            adjectif = String(adjectif.split(separator: "°")[voyelle ? 1 : 0])
        }
        
        if indef && pluriel && prePose {
          article = voyelleAdj ? "d'" : "de"
        }
        
        if (voyelle && noAdj) || (voyelle && !noAdj && !prePose) || (!noAdj && prePose && voyelleAdj) {
            article = article.replacingFirstOccurrence(of: ".\\* ", with: "'", options: .regularExpression)
            article = article.replacingFirstOccurrence(of: "¤", with: "t")
        }
        
        if noAdj {
            cObj = article + " " + nom
        } else {
            cObj = article
            if !noAdj && prePose {
                let withQuote = article.contains("'")
                cObj += withQuote ? adjectif : " " + adjectif
            }
            cObj += " " + nom;
            if !noAdj && !prePose {
                cObj += " " + adjectif;
            }
        }
        
        if (voyelle && noAdj) || (voyelle && !noAdj && !prePose) || (!noAdj && prePose && voyelleAdj) {
            cObj = cObj.replacingFirstOccurrence(of: ".\\* ", with: "'", options: .regularExpression)
            cObj = cObj.replacingFirstOccurrence(of: "¤", with: "t")
        }
        cObj = cObj.replacingOccurrences(of: "[\\*¤]", with: "", options: .regularExpression)

        return cObj
    }
    
    func accordPluriel(mot: String, pluriel: Bool) -> String {
        var resultat = mot
        if pluriel {
            if !resultat.contains("²") {
                resultat += "s"
            } else {
                if !resultat.contains("²²") {
                    resultat = resultat.replacingOccurrences(of: "²([^\\s])", with: "$1", options: [.regularExpression])
                    resultat = resultat.replacingOccurrences(of: "²", with: "")
                } else {
                    resultat = String(resultat.split(separator: "²²")[1])
                }
            }
        } else {
            if resultat.contains("²²") {
                resultat = String(resultat.split(separator: "²²")[0])
            }
            resultat = resultat.replacingOccurrences(of: "²[^\\s]", with: "", options: [.regularExpression])
            resultat = resultat.replacingOccurrences(of: "²", with: "")
        }
        return resultat
    }
}

// Romain Valeri Phrase
extension ValeriGenerator {
    func assembler(mots: [String], questionInv: Bool = false) -> String {
        var resultat = mots.joined(separator: " ")
        resultat = resultat.replacingOccurrences(of: " ,", with: ",", options: [.literal])
        resultat = resultat.replacingOccurrences(of: "' ", with: "'", options: [.literal])
        resultat = resultat.replacingOccurrences(of: "à [Ll]es ", with: "aux ", options: [.regularExpression])
        resultat = resultat.replacingOccurrences(of: "à [Ll]e ", with: "au ", options: [.regularExpression])
        resultat = resultat.replacingOccurrences(of: " en [Ll]e ", with: " en ", options: [.regularExpression])
        resultat = resultat.replacingOccurrences(of: " de ([\(ValeriGenerator.voyelleListe)])", with: " d'$1", options: [.regularExpression, .caseInsensitive])
        resultat = resultat.replacingOccurrences(of: " que ([\(ValeriGenerator.voyelleListe)])", with: " qu'$1", options: [.regularExpression, .caseInsensitive])
        resultat = resultat.replacingOccurrences(of: "plus des ([\(ValeriGenerator.voyelleListe)])", with: "plus d'$1", options: [.regularExpression, .caseInsensitive])
        resultat = resultat.replacingOccurrences(of: " de [Ll]es ", with: " des ", options: [.regularExpression])
        resultat = resultat.replacingOccurrences(of: " de de(s)? ([\(ValeriGenerator.voyelleListe)])", with: " d'$2", options: [.regularExpression, .caseInsensitive])
        resultat = resultat.replacingOccurrences(of: " de de(s)? ([^\(ValeriGenerator.voyelleListe)])", with: " de $2", options: [.regularExpression, .caseInsensitive])
        resultat = resultat.replacingOccurrences(of: " de [Dd]'", with: " d'", options: [.regularExpression])
        resultat = resultat.replacingOccurrences(of: " de ([Ll]e|du) ", with: " du ", options: [.regularExpression])
        if questionInv == false {
            // Valeri (javascript) version replace only the first occurence
            let pattern = "je ([\(ValeriGenerator.voyelleListe)])"
            if let range = resultat.range(of: pattern, options: [.regularExpression, .caseInsensitive]) {
                resultat = resultat.replacingOccurrences(of: pattern, with: "j'$1", options: [.regularExpression, .caseInsensitive], range: range)
            }
        }
        resultat = resultat.replacingOccurrences(of: "£", with: "h", options: [.literal])
        resultat = resultat.replacingOccurrences(of: "µ", with: "Y", options: [.literal])
        resultat = resultat.replacingOccurrences(of: "¥", with: "y", options: [.literal])
        return resultat
    }
    
    func ponctuer(_ phrase: String, _ isAQuestion: Bool) -> String {
        let probaPoint = (isAQuestion ? [0, 0, 0, 1] : ValeriGenerator.proba[.ponctuationPointFinal])!
        var result = phrase.prefix(1).capitalized + phrase.dropFirst()
        result += probaSwitch(ValeriGenerator.data[.ponctuationPointFinal]!, probaPoint)
        return result
    }
    
    func genererPhrase() -> String {
        let generator = ValeriGenerator()

        var mots: [String] = []
        var questionInv = false
        var questionSimple = false
        var structure = generator.genererStructure()
        var pp = false
        var genreSujet = ""
        let tirageModeInterrogatif = de(100)
        if tirageModeInterrogatif > 94 {
            questionSimple = true
        } else if tirageModeInterrogatif > 77 {
            questionInv = true
        }
        var posVerbe = -1; var posVerbe2 = -1; var posModal = -1; var posQue = -1; var posPR = -1
        var personne = 0; var temps = -1
        var premierGN = 0
        let advPost = false // pas modifié, pourrait être supprimé dans les tests où il aparrait
        var flagNoNeg = false
        var particules: [String] = []

        let preAdverbeCheck = structure.contains("AP")
        
        /* --- BOUCLE SUR LES BLOCS DE LA STRUCTURE --- */
        for i in 0..<structure.count {
            if structure[i].first == "§" {
                var litteral = String(structure[i].dropFirst())
                if litteral.contains("/") {
                    litteral = String(litteral.split(separator: "/")[(de(2) - 1)])
                }
                mots.append(litteral)
                if litteral == "sans" {
                    flagNoNeg = true
                }
            } else {
                var mot = ""
                var chercheParticule = false
                repeat {
                    switch structure[i] {
                    case "GN":
                        if de(11) > 1 {
                            mot = generator.groupeNominal(estObjet: premierGN > 0)
                            if mot.contains("_PP") {
                                pp = true
                                mot = mot.replacingFirstOccurrence(of: "_PP", with: "")
                            }
                        } else {
                            mot = generator.recupererMot(code: .GN) ?? "GN vide"
                        }
                        if(premierGN == 0) {
                            genreSujet = mot.contains("__F") ? "F" : "H"
                            premierGN = i + 1
                        }
                        mot = mot.replacingFirstOccurrence(of: "__F", with: "")
                    case "CO":
                        if de(11) > 1 {
                            mot = generator.complementObjet(personneSujet: personne)
                        } else {
                            mot = generator.recupererMot(code: .CO) ?? "CO vide"
                        }
                    case "VET":
                        mot = generator.recupererMot(code: (de(8) > 7) ? .VT: .VET) ?? "VT ou VET vide"
                    case "VAV":
                        mot = generator.recupererMot(code: (de(10) > 9) ? .VT: .VAV) ?? "VT ouVAV vide"
                    default:
                        if let code = ValeriGenerator.Code(rawValue: structure[i]) {
                            mot = generator.recupererMot(code: code) ?? "\(structure[i]) vide"
                        } else {
                            mot = "Error pas de code '\(structure[i])' dans le generator"
                        }
                    }
                    let regex = /(.*){(.*)}/
                    if let match = mot.firstMatch(of: regex) {
                        chercheParticule = true
                        while particules.count < i {
                            particules.append("")
                        }
                        particules.append(String(match.2))
                        mot = String(match.1)
                    }
                    // ???
                    // Le test original vérifie "(particules = [])" ce qui en javascript
                    // remet à vide le tableau particules. Est-ce que l'auteur voulait
                    // cette réinitialisation avec = ou voulait une comparaison avec == ?
                    //
                    // } while preAdverbeCheck && chercheParticule && (particules = []));
                } while preAdverbeCheck && chercheParticule && particules.isEmpty
                
                if let posPersonne = mot.firstIndex(of: "@") {
                    if personne == 0 {
                        if let i = Int(mot.character(after: posPersonne)) {
                            personne = i
                        } else {
                            personne = 1
                        }
                    }
                    mot = String(mot[..<posPersonne])
                }
                mots.append(mot)
            }
            
            let verbesNonMod = ["VT", "VN", "VOA", "VOD", "VOI", "VTL", "VAV", "VET", "VOS"]
            let verbesMod = ["VM", "VD", "VA"]
            if verbesNonMod.contains(structure[i]) {
                if posVerbe > -1 {
                    if posModal > -1 {
                        posVerbe2 = i
                    } else {
                        posModal = posVerbe
                        posVerbe = i
                    }
                } else {
                    posVerbe = i
                }
            }
            if verbesMod.contains(structure[i]) {
                posModal = i
            }
            if structure[i] == "§que" {
                posQue = i
            }
            if structure[i] == "CT" {
                if let posTemps = mots[i].firstIndex(of: "¤") {
                    temps = Int(mots[i].character(after: posTemps)) ?? 0
                    mots[i] = String(mots[i][..<posTemps])
                }
                while mots[i].contains("$") {
                    var nom: String
                    repeat {
                        nom = generator.puiser(.gnNomPropre)
                        nom = nom.replacingFirstOccurrence(of: "_F", with: "")
                    } while mots[i].contains(nom)
                    mots[i] = mots[i].replacingFirstOccurrence(of: "$", with: nom)
                }
                mots[i] = mots[i].replacingOccurrences(of: " de ([\(ValeriGenerator.voyelleListe)])", with: " d'$1", options: [.regularExpression, .caseInsensitive])
            }
            if (structure[i] == "CL") || (structure[i] == "AF") {
                while mots[i].contains("$") {
                    var nom: String
                    repeat {
                        nom = generator.puiser(.gnNomPropre)
                        nom = nom.replacingFirstOccurrence(of: "_F", with: "")
                    } while mots[i].contains(nom)
                    mots[i] = mots[i].replacingFirstOccurrence(of: "$", with: nom)
                }
                mots[i] = mots[i].replacingOccurrences(of: " de ([\(ValeriGenerator.voyelleListe)])", with: " d'$1", options: [.regularExpression, .caseInsensitive])
                while mots[i].contains("+") {
                    let posPlus = mots[i].firstIndex(of: "+")!
                    let fem = mots[i].character(after: posPlus) == "F"
                    var nom: String = ""
                    var ok: Bool
                    repeat {
                        ok = true
                        nom = generator.puiser(.gnNomCommun)
                        if let pos = nom.firstIndex(of: "_") {
                            let genre = nom.character(after: pos)
                            if (!fem && genre == "F") || (fem && genre == "H") {
                                ok = false
                            } else {
                                nom = String(nom.split(separator: "_")[0])
                            }
                        } else {
                            var parts = nom.split(separator: "%")
                            if parts[1] == "e" {
                                parts[1] = parts[0] + "e"
                            }
                            nom = String(fem ? parts[1] : parts[0])
                        }
                    } while !ok || (nom == "")
                    nom = generator.accordPluriel(mot: nom, pluriel: false)
                    mots[i] = mots[i].replacingFirstOccurrence(of: "\\+[FH]", with: nom, options: .regularExpression)
                }
                mots[i] = mots[i].replacingOccurrences(of: " de ([\(ValeriGenerator.voyelleListe)])", with: " d'$1", options: [.regularExpression, .caseInsensitive])
            }
            if (structure[i] == "CL") || (structure[i] == "CT") || (structure[i] == "AF") {
                while mots[i].contains("&") {
                    let pos = mots[i].firstIndex(of: "&")!
                    let suite = mots[i][pos..<mots[i].endIndex]
                    var nombre = de(10) + 1
                    if suite.hasPrefix("&0") {
                        nombre = (nombre * 10) - 10
                    }
                    if suite.hasPrefix("&00") {
                        nombre *= 10
                    }
                    let lettres = nombreEnLettres(nombre) ?? "mauvais nombre"
                    mots[i] = mots[i].replacingFirstOccurrence(of: "&(0){0,2}", with: lettres, options: .regularExpression)
                }
            }
            if structure[i].hasPrefix("PR_") {
                posPR = i
            }
        } /* --- FIN DE LA BOUCLE SUR LES BLOCS DE LA STRUCTURE --- */

        if temps == -1 {
            temps = (de(2) > 1) ? 2 : de(3)
        }

        if questionInv && mots[premierGN - 1] == "je" && temps == 2 && de(5) > 2 {
            questionInv = false
            questionSimple = true
        }

        if posQue > -1 {
            mots[posQue] = beginWithVowel(mots[posQue + 1]) ? "qu'" : "que"
        }

        var tPers = [2]
        if posPR > -1 {
            if mots[posPR].contains(/^s(\'|e )/) {
                tPers.append(personne)
            }
            var neg1 = ""
            var neg2 = ""
            if !flagNoNeg && de(13) > 12 {
                neg1 = beginWithVowel(mots[posPR]) ? "n'" : "ne "
                neg2 = " " + probaSwitch(ValeriGenerator.data[.negation]!, ValeriGenerator.proba[.negation]!)
            }
            mots[posPR] = "en " + neg1 + generator.conjuguer(mots[posPR], temps: 4, pers: tPers, questionInv: false, genreSujet: genreSujet) + neg2
        }
        if posModal > -1 {
            var baseVerbe = generator.conjuguer(mots[posModal], temps: temps, pers: [personne], questionInv: questionInv, genreSujet: genreSujet)
            if !flagNoNeg && !advPost && de(13) > 12 {
                let voyelle = beginWithVowel(baseVerbe)
                let neg = probaSwitch(ValeriGenerator.data[.negation]!, ValeriGenerator.proba[.negation]!)
                baseVerbe = (voyelle ? "n'" : "ne ") + baseVerbe + " " + neg
            }
            mots[posModal] = baseVerbe
            
            if mots[posVerbe].contains("#") {
                mots[posVerbe] = String(mots[posVerbe].split(separator: "#")[0])
            }
            if (personne % 3) != 0 && mots[posVerbe].contains("^s(\'|e )") {
                var pr = ValeriGenerator.data[.pronomReflexif]![personne - 1] + " "
                if mots[posVerbe].contains("'") && personne < 3 {
                    pr = pr.replacingFirstOccurrence(of: "e ", with: "'")
                }
                mots[posVerbe] = mots[posVerbe].replacingFirstOccurrence(of: "^s(\'|e )", with: pr)
            }
            if !flagNoNeg && de(13) > 12 {
                let neg2 = probaSwitch(ValeriGenerator.data[.negation]!, ValeriGenerator.proba[.negation]!)
                mots[posVerbe] = "ne " + neg2 + " " + mots[posVerbe]
            }
            if posVerbe2 > -1 {
                if mots[posVerbe2].contains("#") {
                    mots[posVerbe2] = String(mots[posVerbe2].split(separator: "#")[0])
                }
                if (personne % 3) != 0 && mots[posVerbe2].contains("^s(\'|e )") {
                    var pr = ValeriGenerator.data[.pronomReflexif]![personne - 1] + " "
                    if mots[posVerbe2].contains("'") && personne < 3 {
                        pr = pr.replacingFirstOccurrence(of: "e ", with: "'")
                    }
                    mots[posVerbe2] = mots[posVerbe2].replacingFirstOccurrence(of: "^s(\'|e )", with: pr)
                }
                if !flagNoNeg && de(13) > 12 {
                    let neg2 = probaSwitch(ValeriGenerator.data[.negation]!, ValeriGenerator.proba[.negation]!)
                    mots[posVerbe2] = "ne " + neg2 + " " + mots[posVerbe2]
                }
            }
        } else if posVerbe > -1 {
            var baseVerbe = generator.conjuguer(mots[posVerbe], temps: temps, pers: [personne], questionInv: questionInv, genreSujet: genreSujet)
     // ligne après l'alert, double wtf : la ligne de code en elle-même ET le commentaire ^^'
     // Plus la moindre idée de ce que ça pouvait vouloir signifier mais ça sent la rustine de merde
     // à dégager ASAP
     //    if (baseVerbe === undefined) alert("copine");// préparation du nuke de la copine connasse
     //    if (baseVerbe === undefined) return new Phrase(options);// et sa copine */
            if !flagNoNeg && !advPost && de(13) > 12 {
                let voyelle = beginWithVowel(baseVerbe)
                let neg = probaSwitch(ValeriGenerator.data[.negation]!, ValeriGenerator.proba[.negation]!)
                baseVerbe = (voyelle ? "n'" : "ne ") + baseVerbe + " " + neg
            }
            mots[posVerbe] = baseVerbe
        }
        for i in 0..<particules.count {
            if !particules[i].isEmpty {
                mots[i] += " " + particules[i]
            }
        }
        if questionInv {
            if pp {
                if premierGN > 0 {
                    mots.remove(at: premierGN - 1)
                    structure.remove(at: premierGN - 1)
                    structure[premierGN - 1] += "-GN"
                }
            }
            if de(2) > 1 {
                var adjectifsDispo = ValeriGenerator.data[.adjectifInterrogatif]!
                var probas = ValeriGenerator.proba[.adjectifInterrogatif]!
                if !structure.contains("CT") {
                    adjectifsDispo.append(contentsOf: ValeriGenerator.data[.adjectifInterrogatifTemps]!)
                    probas.append(contentsOf: ValeriGenerator.proba[.adjectifInterrogatifTemps]!)
                }
                if !structure.contains("CL") {
                    adjectifsDispo.append(contentsOf: ValeriGenerator.data[.adjectifInterrogatifLieu]!)
                    probas.append(contentsOf: ValeriGenerator.proba[.adjectifInterrogatifLieu]!)
                }
                if !structure.contains("AP") && !structure.contains("AF") {
                    adjectifsDispo.append(contentsOf: ValeriGenerator.data[.adjectifInterrogatifManiere]!)
                    probas.append(contentsOf: ValeriGenerator.proba[.adjectifInterrogatifManiere]!)
                }
                mots.insert(probaSwitch(adjectifsDispo, probas), at: premierGN - 1)
                structure.insert("AI", at: premierGN - 1)
            }
        }
        
        if questionSimple {
            mots.insert("est-ce que", at: premierGN - 1)
            structure.insert("QS", at: premierGN - 1)
        }
        
        var phrase = assembler(mots: mots, questionInv: questionInv)
        phrase = ponctuer(phrase, questionSimple || questionInv)
        return phrase
    }
        
    func genererParagraphe() -> String {
        let numberOfPhrases = Int.random(in: 3..<8)
        var phrases: [String] = []
        for _ in 1...numberOfPhrases {
            phrases.append(genererPhrase())
        }
        return phrases.joined(separator: " ")
    }
}
