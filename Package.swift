// swift-tools-version: 6.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "StringGenerator",
    defaultLocalization: "en",
    platforms: [
        .macOS(.v13),
        .iOS(.v17)
    ],
    products: [
        .library(
            name: "StringGenerator",
            targets: ["StringGenerator"])
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "StringGenerator",
            dependencies: []),
        .testTarget(
            name: "StringGeneratorTests",
            dependencies: ["StringGenerator"])
    ]
)
